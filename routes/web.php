<?php

Route::group(['middleware' => 'web'], function() {

    Route::get('/', 'HomeController@index')->name('home');
	Route::get('/home', 'HomeController@index')->name('home');
	
	// Just For Administrator
	Route::group(['middleware' => ['ceklevel:1', 'ceklevel:2']], function() {
		
		Route::get('students/certificates', 'StudentController@certificate')->name('students.certificate');
		Route::get('students/certificate/{id}', 'StudentController@set_certificate')->name('students.set_certificate');
		Route::put('students/certificate/{id}', 'StudentController@update_certificate')->name('students.update_certificate');
		Route::get('students/print/ktm/{id}', 'StudentController@printKTM')->name('students.print_ktm');
		Route::get('students/print/ktm', 'StudentController@show_KTM')->name('students.ktm');
		Route::resource('/students', 'StudentController');

		Route::resource('/curriculums', 'CurriculumController');
		Route::resource('/departments', 'DepartmentController');
		Route::resource('/rooms', 'RoomController');
		Route::resource('/lecturers', 'LecturerController');
		Route::resource('/courses', 'CourseController');
		Route::resource('/schedules', 'ScheduleController');
		Route::resource('/years', 'YearController');

		Route::get('/users/profile', 'UserController@profile');
		Route::put('/users/profile/{id}', 'UserController@change_profile')->name('profile.update');
		Route::resource('/users', 'UserController');

		Route::get('/grades', 'GradeController@index')->name('grades.index');
		Route::post('/grades', 'GradeController@store')->name('grades.store');
		Route::get('/grades/{id}/edit', 'GradeController@edit')->name('grades.edit');
		Route::get('/grades/student/{set_nilai}/edit', 'GradeController@set_nilai')->name('grades.set_nilai');
	});


	// Just For Students
	Route::group(['middleware' => 'ceklevel:3'], function() {
		Route::get('/user/student/profile', 'StudentController@detail')->name('students.detail');
	    Route::get('/user/student/profile/edit', 'StudentController@edit_profile')->name('students.edit_profile');
	    Route::put('/students/{student}', 'StudentController@update')->name('students.update');
		
		Route::get('/user/student/password/edit', 'UserController@edit_password')->name('users.edit_password');
		Route::put('/users/{user}', 'UserController@update')->name('users.update');

		Route::get('/user/student/krs', 'KRSController@index')->name('krs.index');
		Route::post('/user/student/krs', 'KRSController@update_krs')->name('krs.update_krs');
		Route::get('/user/student/krs/detail', 'KRSController@detail')->name('krs.detail');
		Route::get('/user/student/krs/detail/print', 'KRSController@print')->name('krs.print');

		Route::get('/user/student/khs', 'KHSController@index')->name('khs.index');
		Route::get('/user/student/khs/detail/print', 'KHSController@print')->name('khs.print');
		Route::post('/user/student/khs', 'KHSController@store')->name('khs.store');

		Route::get('/user/student/biodata', 'DetailController@biodata')->name('detail.biodata');
		Route::get('/user/student/address', 'DetailController@address')->name('detail.address');
		Route::get('/user/student/family', 'DetailController@family')->name('detail.family');
		Route::get('/user/student/uploads', 'DetailController@upload')->name('detail.upload');
		Route::get('/user/student/guardian', 'DetailController@guardian')->name('detail.guardian');
	});
	
	// Just For Lectures
	Route::group(['middleware' => 'ceklevel:4'], function() {
	   	Route::get('/user/lecturer/profile', 'LecturerController@detail')->name('lecturers.detail');
	    Route::get('/user/lecturer/profile/edit', 'LecturerController@edit_profile')->name('lecturers.edit_profile');
	    Route::put('/lecturers/{lecturer}', 'LecturerController@update')->name('lecturers.update');
		
		Route::get('/user/lecturer/password/edit', 'UserController@edit_password')->name('users.edit_password');
		Route::put('/users/{user}', 'UserController@update')->name('users.update');

		Route::resource('/schedules', 'ScheduleController');
		
		Route::get('/grades', 'GradeController@index')->name('grades.index');
		Route::post('/grades', 'GradeController@store')->name('grades.store');
		Route::put('/grades/{grade}', 'GradeController@update_now')->name('grades.update_now');
		Route::get('/grades/{id}/edit', 'GradeController@edit')->name('grades.edit');
		Route::get('/grades/student/{set_nilai}/edit', 'GradeController@set_nilai')->name('grades.set_nilai');
		Route::get('/grades/student/{update_nilai}/edit/nilai', 'GradeController@update_nilai')->name('grades.update_nilai');
	});
	
});

Auth::routes();