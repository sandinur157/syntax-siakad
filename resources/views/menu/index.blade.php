@extends('layouts.master')

@section('content')
<div class="panel-header bg-primary-gradient">
    <div class="page-inner py-5">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
            <div>
                <h2 class="text-white pb-2 fw-bold"><i class="fas fa-home"></i> Dashboard</h2>
                <h5 class="text-white op-7 mb-2">Selamat datang Nur Sandi, anda login sebagai 
                    @if (Auth::user()->role_id == 1)
                        Administrator
                        @else        
                        Member
                    @endif
                .</h5>
            </div>
            <div class="ml-md-auto py-2 py-md-0">
                <a href="#" class="btn btn-white btn-border btn-rounded mr-2"><i class="fas fa-cog"></i> Settings</a>
            </div>
        </div>
    </div>
</div>

<div class="page-inner mt--5">
    <div class="row mt--2">
        <div class="col-md-6">
            <div class="card">
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="{{ asset('/assets/img/') }}/{{ Auth::user()->image }}" class="card-img" alt="">
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title">{{ ucfirst(Auth::user()->name) }}</h5>
                            <p class="card-text">{{ Auth::user()->email }}</p>
                            <p class="card-text"><small class="text-muted">{{ Auth::user()->created_at }}</small></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')

@endpush