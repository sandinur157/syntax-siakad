@extends('layouts.master')

@section('content')
<div class="panel-header bg-primary-gradient">
    <div class="page-inner py-5">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
            <div>
                <h2 class="text-white pb-2 fw-bold">Data No. Ijazah</h2>
            </div>
            <div class="ml-md-auto py-2 py-md-0">
                <button class="btn btn-sm btn-border border-white text-white" disabled style="cursor: not-allowed;"><i class="fas fa-plus-circle"></i> Tambah</button>
            </div>
        </div>
    </div>
</div>

<div class="page-inner mt--5">
    <div class="row mt--2">
        <div class="col-12">
            @card
                @slot('title')
                    
                @endslot

                @table
                    @slot('thead')
                        <tr>
                            <th>No</th>
                            <th>NIM</th>
                            <th>Nama</th>
                            <th>Tahun Angkatan</th>
                            <th>No.Ijazah</th>
                            <th>Judul Skripsi</th>
                            <th>Aksi</th>
                        </tr>
                    @endslot
                    
                    @php $no = 1; @endphp
                    @foreach ($students as $student)
                        <tr>
                            <td width="5%">{{ $no++ }}.</td>
                            <td>{{ $student->nim }}</td>
                            <td width="25%">{{ $student->nama }}</td>
                            <td width="5%">{{ $student->angkatan }}</td>
                            <td>{{ $student->no_ijazah }}</td>
                            <td>{{ $student->judul_skripsi }}</td>
                            <td width="5%">
                                <div class="form-button-action text-center">
                                    <a href="{{ route('students.set_certificate', $student->id) }}" class="btn btn-primary btn-sm"><i class="far fa-edit"></i> Masukan No. Izajah</a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @endtable
            @endcard
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    $('.table').DataTable({
        "language": {
            "zeroRecords": "<strong>Data tidak ditemukan</strong>"
        }
    })

    $('.card-header').addClass('d-none')
</script>
@endpush