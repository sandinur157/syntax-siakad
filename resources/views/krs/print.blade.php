<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Cetak KRS</title>
	<link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">

	<style>
		* {
			font-family: 'Times New Roman';
			color: #000;
		}
		table {
			border-color: #000;
		}
		span {
			display: block;
			font-size: 12px;
		}
		.card-body td {
			font-size: .8em;
		}
		.card-title {
			font-family: sans-serif;
		}
		.header-detail td {
			font-weight: bold;
			font-size: 16px;
		}
		.heading td {
			font-weight: bold;
		}
		.table-content tr td {
			font-size: 14px;
		}
		.small td {
			font-size: 12px;
		}
		.medium td {
			font-size: 14px;
		}
	</style>
</head>
<body onload="window.print()">
	<div class="container">
		<div class="row justify-content-center mt-5	">
			<div class="col-md-12">
				<div class="card mb-3"	>
					<div class="card-header p-0" style="overflow: hidden;">
						 <img src="{{ asset('assets/img/kop.jpg') }}" alt="" width="100%">
					</div>

					<div class="row no-gutters">
						<div class="col">
							<div class="card-body">
								<h5 class="card-title text-center font-weight-bold">KARTU RENCANA STUDI (KRS)</h5>
								<table width="100%" class="header-detail" style="margin-left: 5%">
									<tr>
										<td width="15%">Nama</td>
										<td width="40%" class="text-uppercase">: {{ $student->nama_mahasiswa }}</td>
										<td width="20%">Semester</td>
										<td width="20%">: {{ $student->semester }}</td>
									</tr>
									<tr>
										<td>NIM</td>
										<td>: {{ $student->nim }}</td>
										<td>TA Akademik</td>
										<td>: {{ $student->angkatan -1 }}/{{ $student->angkatan }}</td>
									</tr>
									<tr>
										<td>Dosen Wali</td>
										<td>: -</td>
										<td colspan="2"><em>SUDAH DISETUJUI WALI</em></td>
									</tr>
								</table>

								<table class="mt-3 table-content" width="95%" border="1" cellpadding="3" style="margin: auto 2.5%;">
									<tr style="background-color: #ddd" class="heading">
										<td class="text-center">No</td>
										<td>Kode MK</td>
										<td>Mata Kuliah</td>
										<td class="text-center">SKS</td>
										<td class="text-center">SMT</td>
										<td class="text-center">Ket</td>
										<td class="text-center">Kelas</td>
										<td>Dosen</td>
									</tr>
									@php 
									$no = 1; 
									$sks = 0;
									@endphp
									@foreach ($courses as $course)
										<tr>
											<td width="3%" class="text-center">{{ $no++ }}</td>
											<td>{{ $course->kode_mk }}</td>
											<td class="text-uppercase">{{ $course->nama_mk }}</td>
											<td class="text-center" width="5%">{{ $course->sks }}</td>
											<td class="text-center" width="5%">{{ $course->semester }}</td>
											<td class="text-center" width="5%">{{ substr(ucfirst($course->ket), 0, 1) }}</td>
											<td class="text-center">{{ $student->nama_kelas }}</td>
											<td>{{ $course->nama_dosen }}</td>
											@php  
					                        $sks += $course->sks;
					                        @endphp
										</tr>
									@endforeach
									<tr>
										<td colspan="3" align="right">Jumlah SKS</td>
										<td>{{ $sks }}</td>
									</tr>
								</table>

								<table width="95%" style="margin-left: 5%;">
									<tr class="small">
										<td colspan="2"></td>
										<td>Cirebon, 31 Oktober 2014</td>
									</tr>
									<tr class="small">
										<td>Catatan:</td>
										<td></td>
										<td>Mengetahui dan Menyetujui</td>
									</tr>
									<tr class="small">
										<td>- KRS harap dibawa setiap mengikuti UTS dan UAS</td>
										<td>Mahasiswa ybs</td>
										<td>Dosen Wali</td>
									</tr>
									<tr class="small">
										<td colspan="3">- Kolom Ket. B (Baru), P (Perbaikan), U (Ulang)</td>
									</tr>
									<tr>
										<td colspan="3" style="visibility: hidden;">$2y$10$v38CO6FaWHNWk1WUht2TaeDHENpfQ2o1042QhdFKFzdxjkYqvyLge</td>
									</tr>
									<tr>
										<td colspan="3" style="visibility: hidden;">$2y$10$v38CO6FaWHNWk1WUht2TaeDHENpfQ2o1042QhdFKFzdxjkYqvyLge</td>
									</tr>
									<tr class="medium">
										<td></td>
										<td class="font-weight-bold">{{ $student->nama_mahasiswa }}</td>
										<td class="font-weight-bold">Yucina Wilandari, M.Si.</td>
									</tr>
									<tr class="medium">
										<td></td>
										<td>NIM. {{ $student->nim }}</td>
										<td>NIP. 1970052347878</td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>