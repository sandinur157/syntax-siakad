@extends('layouts._students.master')

@push('css')
    <style>
        .card .card-header {
            /*border-bottom: 1px solid #fff !important;*/
        }
        .table-name tr td {
            font-weight: bold;
        }
    </style>
@endpush

@section('content')
<div class="page-inner mt-2">
    <div class="col-12">
        
        @card
            @slot('title')
            <strong>Kartu Rencana Studi</strong>
            @endslot
            

            <div class="form-group row">
                <label for="nim" class="col-sm-2 col-form-label">NIM</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" id="nim" name="nim" value="{{ $student->nim }}" readonly>
                </div>
            </div>
            <div class="form-group row">
                <label for="nama" class="col-sm-2 col-form-label">Nama</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" id="nama" name="nama" value="{{ $student->nama_mahasiswa }}" readonly>
                </div>
            </div>
            <div class="form-group row">
                <label for="kode_jurusan" class="col-sm-2 col-form-label">Jurusan</label>
                <div class="col-sm-4">
                    <input class="form-control" value="{{ $student->nama_jurusan }}" readonly>
                    <input type="text" class="form-control d-none" id="kode_jurusan" name="kode_jurusan" value="{{ $student->kode_jurusan }}" readonly>
                </div>
            </div>
            <div class="form-group row">
                <label for="kelas" class="col-sm-2 col-form-label">Kelas</label>
                <div class="col-sm-3">
                    <input type="text" class="form-control d-none" name="kelas_id" value="{{ $student->kelas_id }}">
                    <input class="form-control" id="kelas" value="{{ $student->nama_kelas }}" readonly>
                </div>
            </div>

            <div class="form-group row">
                <label for="semester" class="col-sm-2 col-form-label">Semester Yang Ditempuh</label>
                <div class="col-sm-2">
                    <input type="text" class="form-control" id="semester" name="semester" value="{{ $student->semester }}" readonly>
                </div>
            </div>
            <div class="form-group row">
                <label for="tahun_akad" class="col-sm-2 col-form-label">tahun_akad</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" id="tahun_akad" name="tahun_akad" value="{{ $student->angkatan -1 }}/{{ $student->angkatan }}" readonly>
                </div>
            </div>
        @endcard

        @card
        <form action="{{ route('krs.update_krs') }}" method="post">
            @csrf
            
            <input type="hidden" class="form-control" id="nim" name="nim" value="{{ $student->nim }}">
            @slot('title')
            <strong>Data Mata Kuliah Yang akan Di Tempuh</strong> <br>
            <p class="d-inline">Tahun Akademik <strong>{{ $student->angkatan -1 }}/{{ $student->angkatan }}</strong></p>
            @endslot
            
            @table
                @slot('thead')
                    <tr>
                        <th>Pilih</th>
                        <th>No</th>
                        <th>Kode Matkul</th>
                        <th>Mata Kuliah</th>
                        <th>Kelas</th>
                        <th>Jurusan</th>
                        <th>SKS</th>
                        <th>Dosen</th>
                        <th>Ruang</th>
                        <th>Hari</th>
                        <th>Jam</th>                
                    </tr>
                @endslot
                
                @php $no = 1; @endphp
                @foreach ($courses as $course)
                <tr>
                    <td width="5%"><input type="checkbox" name="pilih[]" value="{{ $course->kode_mk }}"></td>
                    <td>{{ $no++ }}</td>
                    <td>
                        <div class="form-group">
                            <input class="form-control form-control-sm" value="{{ $course->kode_mk }}" readonly>
                        </div>
                    </td>
                    <td>{{ $course->nama_mk }}</td>
                    <td>{{ $course->nama_kelas }}</td>
                    <td>{{ $course->nama_jurusan }}</td>
                    <td>{{ $course->sks }}</td>
                    <td>{{ $course->nama_dosen }}</td>
                    <td>{{ $course->nama_ruang }}</td>
                    <td>{{ $course->hari }}</td>
                    <td><span class="badge badge-dark rounded" style="font-size: .8em">{{ $course->jam_mulai }} - {{ $course->jam_selesai }} WIB</span></td>
                </tr>
                @endforeach
            @endtable
            <div class="card-footer">
                <div class="row">
                    <div class="col">
                        @if (!empty($count))
                            <button class="btn btn-primary btn-sm"> <i class="fas fa-plus-circle"></i> Tambah</button>
                        @else
                            <button class="btn btn-primary btn-sm" disabled style="cursor: not-allowed;"> <i class="fas fa-plus-circle"></i> Tambah</button>
                        @endif
                        <a href="{{ url('/user/student/krs/detail') }}" class="btn btn-default btn-sm ml-3"><i class="far fa-eye"></i> Lihat KRS</a>
                    </div>                                      
                </div>
            </div>
        </form>
        @endcard
    </div>
</div>
@endsection

@push('scripts')
<script>
    $('.table').DataTable({
        "language": {
            "zeroRecords": "<strong>Data tidak ditemukan</strong>"
        }
    })

    $('.wrapper').addClass('sidebar_minimize')
    $('label.offset-sm-1').append(` <span class="required-label">*</span>`)
    $('.form-group .form-control').addClass('form-control-sm')
</script>
@endpush