@extends('layouts.auth')

@section('content')
<form action="" method="post">
    @csrf
    <div class="container container-login container-transparent animated fadeInRight">
        <h3 class="text-center">Sign In To Admin</h3>
        <div class="login-form">
            <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                <label for="email" class="placeholder"><b>Email</b></label>
                <input type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
            </div>
            <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                <label for="password" class="placeholder"><b>Password</b></label>
                <a href="#" class="link float-right">Forget Password ?</a>
                <div class="position-relative">
                    <input id="password" name="password" type="password" class="form-control" required>
                    <div class="show-password">
                        <i class="icon-eye"></i>
                    </div>
                </div>
            </div>
            <div class="form-group form-action-d-flex mb-3">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="rememberme" name="remember" {{ old('remember') ? 'checked' : '' }}>
                    <label class="custom-control-label m-0" for="rememberme">Remember Me</label>
                </div>
                <button class="btn btn-primary col-md-5 float-right mt-3 mt-sm-0 fw-bold">Sign In</button>
            </div>
            <div class="login-account">
                <span class="msg">Don't have an account yet ?</span>
                <a href="#" id="show-signup" class="link">Sign Up</a>
            </div>
        </div>
    </div>
</form>

@include('auth.register')
@endsection