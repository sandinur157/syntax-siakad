<footer class="footer">
    <div class="container-fluid mt-1">
        <div class="pull-left">
            <strong>Copyright &copy; 2019</strong> All rights reserved, Designed by <a href="https://www.themekita.com">ThemeKita</a>
        </div>
        <div class="copyright ml-auto">
            Developed with <i class="fa fa-heart heart text-danger"></i> by <a href="https://www.nursandi.com">Nursandi</a>
        </div>
    </div>
</footer>