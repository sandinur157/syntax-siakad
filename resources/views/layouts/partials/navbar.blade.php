<div class="main-header">
  <div class="logo-header" data-background-color="blue2">
    <a href="{{ url('/') }}" class="logo">
      <img src="{{ asset('/assets/img/logo.png') }}" alt="navbar brand" class="navbar-brand" width="120">
    </a>
    <button class="navbar-toggler sidenav-toggler ml-auto" type="button" data-toggle="collapse" data-target="collapse"
    aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon">
        <i class="icon-menu"></i>
      </span>
    </button>
    <!-- Show XS -->
    <a class="nav-link topbar-toggler more" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" title="Logout">
      <i class="fa fa-sign-out-alt"></i>
    </a>
    <div class="nav-toggle">
      <button class="btn btn-toggle toggle-sidebar">
        <i class="icon-menu d-none d-sm-block"></i>
      </button>
    </div>
  </div>
  
  <nav class="navbar navbar-header navbar-expand-lg" data-background-color="blue2">
    <div class="container-fluid">
      <div class="collapse" id="search-nav">
        <form class="navbar-left navbar-form nav-search mr-md-3 d-none d-sm-block">
          <div class="input-group">
            <div class="input-group-prepend">
              <button type="submit" class="btn btn-search pr-1">
                <i class="fa fa-search search-icon d-none d-sm-block"></i>
              </button>
            </div>
            <input type="text" placeholder="Search ..." class="form-control">
          </div>
        </form>
      </div>
      <ul class="navbar-nav topbar-nav ml-md-auto align-items-center">
        <li class="nav-item toggle-nav-search hidden-caret">
          <a class="nav-link d-none d-sm-block" data-toggle="collapse" href="#search-nav" role="button" aria-expanded="false" aria-controls="search-nav">
            <i class="fa fa-search"></i>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
            <i class="fa fa-sign-out-alt"></i> Logout
          </a>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none">@csrf</form>
        </li>
      </ul>
    </div>
  </nav>
</div>