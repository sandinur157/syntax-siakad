<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Login - Admin</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="icon" href="{{ asset('/assets/img/icon.ico') }}" type="image/x-icon" />

    <script src="{{ asset('/assets/js/plugin/webfont/webfont.min.js') }}"></script>
    <script>
        WebFont.load({
            google: {
                "families": ["Lato:300,400,700,900"]
            },
            custom: {
                "families": ["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular",
                    "Font Awesome 5 Brands", "simple-line-icons"
                ],
                urls: ['/assets/css/fonts.min.css']
            },
            active: function () {
                sessionStorage.fonts = true;
            }
        });
    </script>

    <link rel="stylesheet" href="{{ asset('/assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/css/atlantis.min.css') }}">
</head>

<body class="login" style="overflow-x: hidden;">

    <div class="wrapper wrapper-login wrapper-login-full p-0"{{--  style="background-image: url('{{ asset('/assets/img/login-bg.jpg') }}');" --}}>

        <div class="login-aside w-50 d-flex flex-column align-items-center justify-content-center bg-primary-gradient text-center">
            <h1 class="title fw-bold text-white mb-3">Join Our Comunity</h1>
            <p class="subtitle text-white op-7">Ayo bergabung dengan komunitas kami untuk masa depan yang lebih baik</p>
        </div>
        
        <div class="login-aside w-50 d-flex align-items-center justify-content-center bg-white">
            @yield('content')
        </div>

    </div>

    <script src="{{ asset('/assets/js/core/jquery.3.2.1.min.js') }}"></script>
    <script src="{{ asset('/assets/js/core/popper.min.js') }}"></script>
    <script src="{{ asset('/assets/js/core/bootstrap.min.js') }}"></script>
    <script src="{{ asset('/assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('/assets/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js') }}"></script>
    <script src="{{ asset('/assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js') }}"></script>
    <script src="{{ asset('/assets/js/plugin/chart.js/chart.min.js') }}"></script>
    <script src="{{ asset('/assets/js/plugin/jquery.sparkline/jquery.sparkline.min.js') }}"></script>
    <script src="{{ asset('/assets/js/plugin/chart-circle/circles.min.js') }}"></script>
    <script src="{{ asset('/assets/js/plugin/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('/assets/js/plugin/bootstrap-notify/bootstrap-notify.min.js') }}"></script>
    <script src="{{ asset('/assets/js/plugin/jqvmap/jquery.vmap.min.js') }}"></script>
    <script src="{{ asset('/assets/js/plugin/jqvmap/maps/jquery.vmap.world.js') }}"></script>
    <script src="{{ asset('/assets/js/plugin/sweetalert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('/assets/js/atlantis.min.js') }}"></script>
</body>

</html>