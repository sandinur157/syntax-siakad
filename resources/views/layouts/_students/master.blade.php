<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SYNTAX - SATU</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="icon" href="{{ asset('/assets/img/icon.ico') }}" type="image/x-icon" />

    <script src="{{ asset('/assets/js/plugin/webfont/webfont.min.js') }}"></script>
    <script>
        WebFont.load({
            google: {
                "families": ["Lato:300,400,700,900"]
            },
            custom: {
                "families": ["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular",
                    "Font Awesome 5 Brands", "simple-line-icons"
                ],
                urls: ['{{ asset('/assets/css/fonts.min.css') }}']
            },
            active: function () {
                sessionStorage.fonts = true;
            }
        });
    </script>

    <link rel="stylesheet" href="{{ asset('/assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/css/atlantis2.min.css') }}">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    
    <style>
        .main-header .nav-bottom .page-navigation>.nav-item.submenu>.nav-link:not(.profile):after {
            content: '';
        }
    </style>

    @stack('css')
</head>

<body>

    <div class="wrapper horizontal-layout-2">
        @includeIf('layouts._students.navbar')

        <div class="main-panel">
            <div class="content">
                                
                @yield('content')

            </div>
            
        </div>
        @includeIf('layouts._students.footer')
    </div>

    <!--   Core JS Files   -->
    <script src="{{ asset('assets/js/core/jquery.3.2.1.min.js')}}"></script>
    <script src="{{ asset('assets/js/core/popper.min.js')}}"></script>
    <script src="{{ asset('assets/js/core/bootstrap.min.js')}}"></script>

    <!-- jQuery UI -->
    <script src="{{ asset('assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js')}}"></script>
    <script src="{{ asset('assets/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js')}}"></script>

    <!-- jQuery Scrollbar -->
    <script src="{{ asset('assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js')}}"></script>

    <!-- Moment JS -->
    <script src="{{ asset('assets/js/plugin/moment/moment.min.js')}}"></script>

    <!-- Chart JS -->
    <script src="{{ asset('assets/js/plugin/chart.js/chart.min.js')}}"></script>

    <!-- jQuery Sparkline -->
    <script src="{{ asset('assets/js/plugin/jquery.sparkline/jquery.sparkline.min.js')}}"></script>

    <!-- Chart Circle -->
    <script src="{{ asset('assets/js/plugin/chart-circle/circles.min.js')}}"></script>

    <!-- Datatables -->
    <script src="{{ asset('assets/js/plugin/datatables/datatables.min.js')}}"></script>

    <!-- Bootstrap Notify -->
    <script src="{{ asset('assets/js/plugin/bootstrap-notify/bootstrap-notify.min.js')}}"></script>

    <!-- Bootstrap Toggle -->
    <script src="{{ asset('assets/js/plugin/bootstrap-toggle/bootstrap-toggle.min.js')}}"></script>

    <!-- jQuery Vector Maps -->
    <script src="{{ asset('assets/js/plugin/jqvmap/jquery.vmap.min.js')}}"></script>
    <script src="{{ asset('assets/js/plugin/jqvmap/maps/jquery.vmap.world.js')}}"></script>

    <!-- Google Maps Plugin -->
    <script src="{{ asset('assets/js/plugin/gmaps/gmaps.js')}}"></script>

    <!-- Dropzone -->
    <script src="{{ asset('assets/js/plugin/dropzone/dropzone.min.js')}}"></script>

    <!-- Fullcalendar -->
    <script src="{{ asset('assets/js/plugin/fullcalendar/fullcalendar.min.js')}}"></script>

    <!-- DateTimePicker -->
    <script src="{{ asset('assets/js/plugin/datepicker/bootstrap-datetimepicker.min.js')}}"></script>

    <!-- Bootstrap Tagsinput -->
    <script src="{{ asset('assets/js/plugin/bootstrap-tagsinput/bootstrap-tagsinput.min.js')}}"></script>

    <!-- Bootstrap Wizard -->
    <script src="{{ asset('assets/js/plugin/bootstrap-wizard/bootstrapwizard.js')}}"></script>

    <!-- jQuery Validation -->
    <script src="{{ asset('assets/js/plugin/jquery.validate/jquery.validate.min.js')}}"></script>
    <script src="{{ asset('assets/js/plugin/validator/validator.min.js')}}"></script>

    <!-- Summernote -->
    <script src="{{ asset('assets/js/plugin/summernote/summernote-bs4.min.js')}}"></script>

    <!-- Select2 -->
    <script src="{{ asset('assets/js/plugin/select2/select2.full.min.js')}}"></script>

    <!-- Sweet Alert -->
    <script src="{{ asset('assets/js/plugin/sweetalert/sweetalert.min.js')}}"></script>

    <!-- Owl Carousel -->
    <script src="{{ asset('assets/js/plugin/owl-carousel/owl.carousel.min.js')}}"></script>

    <!-- Magnific Popup -->
    <script src="{{ asset('assets/js/plugin/jquery.magnific-popup/jquery.magnific-popup.min.js')}}"></script>

    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>

    <!-- Atlantis JS -->
    <script src="{{ asset('assets/js/atlantis2.min.js')}}"></script>
    <script>
        $('.sidebar-content .nav-primary a').filter(function() {
            return this.href == window.location.href;
        }).parent().addClass('active');
    </script>
    

    @stack('scripts')
</body>

</html>