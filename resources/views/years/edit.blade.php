@extends('layouts.master')

@section('content')
<div class="page-inner mt-2">
    <div class="col-12">
        @card
            @slot('title')
                Edit Data Tahun Akademik
            @endslot
            
            <form method="post" action="{{ route('years.update', $year->id) }}">
                @csrf @method('put')
                <div class="form-group row">
                    <label for="tahun" class="col-sm-2 col-form-label">Tahun Akademik</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="tahun" name="tahun" required value="{{ $year->tahun }}">
                    </div>
                </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col-sm-6 offset-sm-3">
                            <button class="btn btn-primary"> <i class="far fa-save"></i> Simpan</button>
                            <a href="{{ url('/years') }}" class="btn btn-danger ml-3"><i class="fas fa-arrow-circle-left"></i> Kembali</a>
                        </div>                                      
                    </div>
                </div>
            </form>
        @endcard
    </div>
</div>
@endsection

@push('scripts')
<script>
    $('label.col-sm-2').addClass('offset-sm-1')
    $('label.offset-sm-1').append(` <span class="required-label">*</span>`)
    $('.form-group .form-control').addClass('form-control-sm')
</script>
@endpush