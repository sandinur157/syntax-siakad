@extends('layouts.master')

@section('content')
<div class="panel-header bg-primary-gradient">
    <div class="page-inner py-5">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
            <div>
                <h2 class="text-white pb-2 fw-bold">Daftar Dosen</h2>
            </div>
            <div class="ml-md-auto py-2 py-md-0">
                <a href="{{ route('lecturers.create') }}" class="btn btn-white btn-sm btn-border"><i class="fas fa-plus-circle"></i> Tambah</a>
            </div>
        </div>
    </div>
</div>

<div class="page-inner mt--5">
    <div class="row mt--2">
        <div class="col-12">
            @card
                @slot('title')
                    
                @endslot

                @table
                    @slot('thead')
                        <th>No</th>
                        <th>Kode Dosen</th>
                        <th>Nama</th>
                        <th>Telpon</th>
                        <th>Email</th>
                        <th>Alamat</th>
                        <th>Foto</th>
                        <th>Aksi</th>
                    @endslot
                    
                    @php $no = 1; @endphp
                    @foreach ($lecturers as $lecturer)
                        <tr>
                            <td width="5%">{{ $no++ }}.</td>
                            <td width="15%">{{ $lecturer->kode }}</td>
                            <td width="25%">{{ $lecturer->nama }}</td>
                            <td>{{ $lecturer->telpon }}</td>
                            <td>{{ $lecturer->email }}</td>
                            <td>{{ $lecturer->alamat }}</td>
                            <td>
                                <img src="{{ asset('assets/img/uploads/') }}/{{ $lecturer->foto }}" alt="profile dosen" class="rounded-circle" height="30">
                            </td>
                            <td width="5%">
                                <form method="POST" action="{{ route('lecturers.destroy', $lecturer->id) }}">
                                    @csrf @method('delete')
                                    <div class="form-button-action text-center">
                                        <a href="{{ route('lecturers.edit', $lecturer->id) }}" class="btn btn-link btn-primary" title="Edit"><i class="far fa-edit"></i></a>
                                        <button class="btn btn-link btn-danger" title="Delete" onclick="return confirm('Yakin?')"><i class="fas fa-trash-alt"></i></button>
                                        <a href="{{ route('lecturers.show', $lecturer->id) }}" class="btn btn-link btn-default" title="Detail"><i class="fas fa-search-plus"></i></a>
                                    </div>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                @endtable
            @endcard
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    $('.table').DataTable({
        "language": {
            "zeroRecords": "<strong>Data tidak ditemukan</strong>"
        }
    })

    $('.card-header').addClass('d-none')
    $('.table').addClass('table-responsive')
</script>
@endpush