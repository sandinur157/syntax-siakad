@extends('layouts.master')

@section('content')
<div class="page-inner mt-2">
    <div class="col-12">
        @card
            @slot('title')
                Detail Dosen
            @endslot

            <div class="card mb-3">
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="{{ asset('/assets/img/uploads/') }}/{{ $lecturer->foto }}" class="card-img" alt="">
                    </div>
                    <div class="col-md-8">
                        <div class="card-body mt--3">
                            <table width="50%" class="table table-bordered">
                                <tr>
                                    <td>Nama</td>
                                    <td>: {{ ucfirst($lecturer->nama) }}</td>
                                </tr>
                                <tr>
                                    <td>No Telpon</td>
                                    <td>: {{ $lecturer->telpon }}</td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td>: {{ $lecturer->email }}</td>
                                </tr>
                                <tr>
                                    <td>Kode Dosen</td>
                                    <td>: {{ $lecturer->kode }}</td>
                                </tr>
                                <tr>
                                    <td>NIDN / NIK</td>
                                    <td>: {{ $lecturer->nidn }}</td>
                                </tr>
                                <tr>
                                    <td>S1</td>
                                    <td>: {{ $lecturer->pend_s1 }}</td>
                                </tr>
                                <tr>
                                    <td>S2</td>
                                    <td>: {{ $lecturer->pend_s2 }}</td>
                                </tr>
                                <tr>
                                    <td>S3</td>
                                    <td>: {{ $lecturer->pend_s3 }}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <a href="{{ url('/lecturers') }}" class="btn btn-danger ml-3"><i class="fas fa-arrow-circle-left"></i> Kembali</a>
            </div>
        @endcard
    </div>
</div>
@endsection

@push('scripts')
<script>
    $('.profile').parent().addClass('active');
</script>
@endpush