@extends('layouts.master')

@section('content')
<div class="page-inner mt-2">
    <div class="col-12">
        @card
            @slot('title')
            @if (Auth::user()->role_id == 1 || Auth::user()->role_id == 2)
                Edit Data Dosen
            @else
                Edit Profile
            @endif
            @endslot
            
            <form method="post" action="{{ route('lecturers.update', $lecturer->id) }}" enctype="multipart/form-data">
                @csrf @method('put')
                <div class="form-group row">
                    <label for="kode" class="col-sm-2 col-form-label">Kode Dosen</label>
                    <div class="col-sm-2">
                        <input class="form-control" name="kode" id="kode" required value="{{ $lecturer->kode }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="nidn" class="col-sm-2 col-form-label">NIDN/NIK</label>
                    <div class="col-sm-4">
                        <input class="form-control" name="nidn" id="nidn" required value="{{ $lecturer->nidn }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="nama" class="col-sm-2 col-form-label">Nama</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="nama" name="nama" required value="{{ $lecturer->nama }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="pend_s1" class="col-sm-2 col-form-label">Pendidikan S1</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="pend_s1" name="pend_s1" value="{{ $lecturer->pend_s1 }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="pend_s2" class="col-sm-2 col-form-label">Pendidikan S2</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="pend_s2" name="pend_s2" value="{{ $lecturer->pend_s2 }}">
                    </div>
                </div><div class="form-group row">
                    <label for="pend_s3" class="col-sm-2 col-form-label">Pendidikan S3</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="pend_s3" name="pend_s3" value="{{ $lecturer->pend_s3 }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="alamat" class="col-sm-2 col-form-label">Alamat</label>
                    <div class="col-sm-8">
                        <textarea name="alamat" id="alamat" class="form-control" rows="5">{{ $lecturer->alamat }}</textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="telpon" class="col-sm-2 col-form-label">No Telpon</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="telpon" name="telpon" required value="{{ $lecturer->telpon }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="email" class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="email" name="email" required value="{{ $lecturer->email }}">
                    </div>
                </div>
                <div class="form-group  row">
                    <label class="col-sm-2">Upload Image</label>
                    <div class="col-sm-4">
                        <div class="input-file input-file-image mt--3">
                            <input type="file" class="form-control form-control-file" id="foto" name="foto" accept="image/*" value="{{ $lecturer->foto }}">
                            <label for="foto" class="img-circle" style="cursor: pointer;"><i class="fa fa-file-image"></i>
                                <img class="img-upload-preview rounded" height="200" src="{{ asset('assets/img/uploads/') }}/{{ $lecturer->foto }}" alt="preview">
                            </label>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col-sm-6 offset-sm-3">
                            <button class="btn btn-primary"> <i class="far fa-save"></i> Simpan</button>
                            <a href="{{ url('/lecturers') }}" class="btn btn-danger ml-3"><i class="fas fa-arrow-circle-left"></i> Kembali</a>
                        </div>                                      
                    </div>
                </div>
            </form>
        @endcard
    </div>
</div>
@endsection

@push('scripts')
<script>
    $('.profile').parent().addClass('active');
    $('#nidn, #email, #kode').attr('readonly', true)

    $('label.col-sm-2').addClass('offset-sm-1')
    $('label.offset-sm-1').append(` <span class="required-label">*</span>`)
    $('.form-group .form-control').addClass('form-control-sm')
</script>
@endpush