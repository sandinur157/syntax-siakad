@extends('layouts.master')

@section('content')
<div class="page-inner mt-2">
    <div class="col-12">
        @card
            @slot('title')
                Edit Profile
            @endslot

			<form class="form fom-horizontal" method="post" data-toggle="validator" enctype="multipart/form-data" action="{{ route('profile.update', Auth::user()->id) }}">
				@csrf @method('put')
				<div class="card-body">
					<div class="form-group row">
					    <label for="nama" class="col-sm-2 control-label">Nama</label>
					    <div class="col-sm-6">
					    	<input type="text" name="nama" id="nama" class="form-control" value="{{ Auth::user()->name }}" required>
					    </div>
					</div>
					
					<div class="form-group  row">
                    <label class="col-sm-2">Upload Image</label>
                    <div class="col-sm-4">
                        <div class="input-file input-file-image mt--3">
                            <input type="file" class="form-control form-control-file" id="foto" name="foto">
                            <label for="foto" class="img-circle" style="cursor: pointer;"><i class="fa fa-file-image"></i>
                                <img class="img-upload-preview rounded" height="200" src="{{ asset('assets/img/uploads/') }}/{{ Auth::user()->image }}" alt="preview">
                            </label>
                        </div>
                    </div>
                </div>					
				<div class="form-group row">
					<label for="password" class="col-sm-2 control-label">Password</label>
					<div class="col-sm-6">
						<input type="password" name="password" id="password" class="form-control">
						<span class="help-block with-errors"></span>
					</div>
				</div>

				<div class="form-group row">
					<label for="password1" class="col-md-2 control-label">Ulang Password</label>
					<div class="col-md-6">
						<input type="password" name="password1" id="password1" class="form-control" data-match="#password">
						<span class="help-block with-errors"></span>
					</div>
				</div>
				<div class="card-footer">
                    <div class="row">
                        <div class="col-sm-6 offset-sm-2">
                            <button class="btn btn-primary"> <i class="far fa-save"></i> Simpan</button>
                            <a href="{{ url('/') }}" class="btn btn-danger ml-3"><i class="fas fa-arrow-circle-left"></i> Kembali</a>
                        </div>                                      
                    </div>
                </div>			
            </form>	
        @endcard
    </div>
</div>
@endsection

@push('scripts')
<script>
	$(function() {
		// 1. saat password lama diubah
		$('#passwordlama').keyup(function() {
			if($(this).val() != '') $('#password, #password1').attr('required', true);
			else $('#password, #password1').attr('required', false);
		})
	})
</script>
@endpush
