@extends('layouts.master')

@section('content')
<div class="panel-header bg-primary-gradient">
    <div class="page-inner py-5">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
            <div>
                <h2 class="text-white pb-2 fw-bold">Data User</h2>
            </div>
            <div class="ml-md-auto py-2 py-md-0">
                <button class="btn btn-sm btn-border border-white text-white" disabled style="cursor: not-allowed;"><i class="fas fa-plus-circle"></i> Tambah</button>
            </div>
        </div>
    </div>
</div>

<div class="page-inner mt--5">
    <div class="row mt--2">
        <div class="col-12">
            @card
                @slot('title')
                    
                @endslot

                @table
                    @slot('thead')
                        <tr>
                            <th width="30">No</th>
                            <th>User ID</th>
                            <th>Nama User</th>
                            <th>Level</th>
                            <th width="100">Aksi</th>
                        </tr>
                    @endslot
                    
                    @php $no = 1; @endphp
                    @foreach ($users as $user)
                    @if ($user->role_id != 1 && $user->role_id != 2)
                        
                        <tr>
                            <td width="5%">{{ $no++ }}.</td>
                            <td>{{ $user->user_id }}</td>
                            <td width="50%">{{ $user->name }}</td>
                            <td width="10%" class="text-center">
                            	@foreach ($roles as $role)
                            		<span class="badge badge-dark rounded" style="font-size: .7em; cursor: not-allowed;">{{ $role->id == $user->role_id ? $role->role : '' }}</span>
                            	@endforeach
                            </td>
                            <td>
                                <div class="form-button-action text-center">
                                    <a href="{{ route('users.edit', $user->id) }}" class="btn btn-link btn-primary"><i class="far fa-edit"></i></a>
                                </div>
                            </td>
                        </tr>
                    @endif
                    @endforeach
                @endtable
            @endcard
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    $('.table').DataTable({
        "language": {
            "zeroRecords": "<strong>Data tidak ditemukan</strong>"
        }
    })

    $('.card-header').addClass('d-none')
</script>
@endpush