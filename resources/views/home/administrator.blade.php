@extends('layouts.master')

@section('content')
<div class="panel-header bg-primary-gradient">
    <div class="page-inner py-5">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
            <div>
                <h2 class="text-white pb-2 fw-bold"><i class="fas fa-home"></i> Dashboard</h2>
                <h5 class="text-white op-7 mb-2">Selamat datang {{ Auth::user()->name }}, anda login sebagai Administrator.</h5>
            </div>
            <div class="ml-md-auto py-2 py-md-0">
                <a href="{{ url('/settings') }}" class="btn btn-white btn-border btn-rounded mr-2"><i class="fas fa-cog"></i> Settings</a>
            </div>
        </div>
    </div>
</div>

<div class="page-inner mt--5">
    <div class="row mt--2">
        <div class="col-sm-6 col-md-3">
            <div class="card card-stats card-round">
                <div class="card-body ">
                    <div class="row">
                        <div class="col-5">
                            <div class="text-center" style="font-size: 3em;">
                                <i class="fas fa-user-graduate text-warning"></i>
                            </div>
                        </div>
                        <div class="col-7 col-stats">
                            <div class="numbers">
                                <p class="card-category">Total Mahasiswa</p>
                                <h4 class="card-title">{{ $students }}</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-3">
            <div class="card card-stats card-round">
                <div class="card-body ">
                    <div class="row">
                        <div class="col-5">
                            <div class="text-center" style="font-size: 3em;">
                                <i class="fas fa-users text-success"></i>
                            </div>
                        </div>
                        <div class="col-7 col-stats">
                            <div class="numbers">
                                <p class="card-category">Total Dosen</p>
                                <h4 class="card-title">{{ $lecturers }}</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-3">
            <div class="card card-stats card-round">
                <div class="card-body">
                    <div class="row">
                        <div class="col-5">
                            <div class="text-center" style="font-size: 3em;">
                                <i class="fas fa-book text-danger"></i>
                            </div>
                        </div>
                        <div class="col-7 col-stats">
                            <div class="numbers">
                                <p class="card-category">Mata Kuliah</p>
                                <h4 class="card-title">{{ $courses }}</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-3">
            <div class="card card-stats card-round">
                <div class="card-body">
                    <div class="row">
                        <div class="col-5">
                            <div class="text-center" style="font-size: 3em;">
                                <i class="fas fa-chalkboard text-primary"></i>
                            </div>
                        </div>
                        <div class="col-7 col-stats">
                            <div class="numbers">
                                <p class="card-category">Jurusan</p>
                                <h4 class="card-title">{{ $departments }}</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>      
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card full-height">
                <div class="card-header">
                    <div class="card-title">Statistik total pendapatan & pengeluaran</div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-3 d-flex flex-column justify-content-around">
                            <div>
                                <h6 class="fw-bold text-uppercase text-success">Total Income</h6>
                                <h3><span class="fw-bold">Rp.</span> 25.000.000,00-</h3>
                            </div>
                            <div>
                                <h6 class="fw-bold text-uppercase text-danger">Total Spend</h6>
                                <h3><span class="fw-bold">Rp.</span> 10.000.000,00-</h3>
                            </div>
                        </div>
                        <div class="col-md-9 chart-container">
                            <canvas id="barChart"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('/js/script.js')}}"></script>
<script>
    $('.table').DataTable({
        "language": {
            "zeroRecords": "<strong>Data tidak ditemukan</strong>"
        }
    })

    let barChart = document.getElementById('barChart').getContext('2d')
    let myBarChart = new Chart(barChart, {
        type: 'bar',
        data: {
            labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            datasets : [{
                label: "Sales",
                backgroundColor: 'rgb(23, 125, 255)',
                borderColor: 'rgb(23, 125, 255)',
                data: [3, 2, 9, 5, 4, 6, 4, 6, 7, 8, 7, 4],
            }],
        },
        options: {
            responsive: true, 
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            },
        }
    });
</script>
@endpush