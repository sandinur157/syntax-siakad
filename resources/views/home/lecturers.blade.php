@extends('layouts.master')

@section('content')
<div class="panel-header bg-primary-gradient">
    <div class="page-inner py-5">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
            <div>
                <h2 class="text-white pb-2 fw-bold"><i class="fas fa-home"></i> Dashboard</h2>
                <h5 class="text-white op-7 mb-2">Selamat datang {{ ucfirst(Auth::user()->name) }}, anda login sebagai Dosen.</h5>
            </div>
            <div class="ml-md-auto py-2 py-md-0 d-none">
                <a href="{{ url('/settings') }}" class="btn btn-white btn-border btn-rounded mr-2"><i class="fas fa-cog"></i> Settings</a>
            </div>
        </div>
    </div>
</div>

<div class="page-inner mt--5">
    <div class="row mt--2">
        <div class="col-sm-6 col-md-3">
            <div class="card card-stats card-round">
                <div class="card-body ">
                    <div class="row">
                        <div class="col-5">
                            <div class="text-center" style="font-size: 3em;">
                                <i class="fas fa-user-graduate text-warning"></i>
                            </div>
                        </div>
                        <div class="col-7 col-stats">
                            <div class="numbers">
                                <p class="card-category">Total Mahasiswa</p>
                                <h4 class="card-title">4.000,00-</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>    
    </div>
    <div class="row"></div>
</div>
@endsection

@push('scripts')
<script>
    
</script>
@endpush