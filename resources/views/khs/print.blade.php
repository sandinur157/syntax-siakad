<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Cetak KHS</title>
	<link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">

	<style>
		* {
			font-family: 'Times New Roman';
			color: #000;
		}
		table {
			border-color: #000;
		}
		span {
			display: block;
			font-size: 12px;
		}
		.card-body td {
			font-size: .8em;
		}
		.card-title {
			font-family: sans-serif;
		}
		.header-detail td {
			font-weight: bold;
			font-size: 16px;
		}
		.heading td {
			font-weight: bold;
		}
		.table-content tr td {
			font-size: 14px;
		}
		.small td {
			font-size: 12px;
		}
		.medium td {
			font-size: 14px;
		}
	</style>
</head>
<body onload="window.print()">
	<div class="container">
		<div class="row justify-content-center mt-5	">
			<div class="col-md-12">
				<div class="card mb-3"	>
					<div class="card-header p-0" style="overflow: hidden;">
						 <img src="{{ asset('assets/img/kop.jpg') }}" alt="" width="100%">
					</div>

					<div class="row no-gutters">
						<div class="col">
							<div class="card-body">
								<h5 class="card-title text-center font-weight-bold">KARTU HASIL STUDI (KHS)</h5>
								<div class="row">
									<div class="col-md-6">
										<table width="100%" class="header-detail ml-5">
											<tr>
												<td>NIM</td>
												<td>: {{ $student->nim }}</td>
											</tr>
											<tr>
												<td>Nama</td>
												<td class="text-uppercase">: {{ $student->nama_mahasiswa }}</td>
											</tr>
											<tr>
												<td>Semester</td>
												<td>: {{ $student->semester % 2 == 0 ? 'Genap' : 'Ganjil' }}/{{ $student->semester }}</td>
											</tr>
											<tr>
												<td>Program Studi</td>
												<td>: {{ $student->nama_jurusan }}</td>
											</tr>
										</table>
									</div>
									<div class="col-md-6">
										<img src="{{ asset('assets/img/uploads/') }}/{{ $student->foto_student }}" alt="" class="rounded shadow-sm float-right mr-5" width="100">
									</div>
								</div>
								

								<table class="mt-3 mb-2 table-content" width="95%" border="1" cellpadding="3" style="margin: auto 2.5%;">
									<tr style="background-color: #ddd" class="heading">
										<td class="text-center">No</td>
										<td>Kode MK</td>
										<td>Mata Kuliah</td>
										<th class="text-center">SKS</th>
				                        <th class="text-center">Semester</th>        
				                        <th class="text-center">Nilai</th>        
				                        <th class="text-center">Bobot</th>        
				                        <th class="text-center">Mutu</th>  
									</tr>
									<tbody>
										@php 
					                        $no = 1; 
					                        $jml_krs  = 0;
					                        $jml_mutu = 0;
					                    @endphp

					                    @foreach ($courses as $course)
					                    
					                    <!-- Logic PHP -->
					                    @php
					                    $total      = $course->nilaiakhir;

					                    if ($total >= 85 || $total <= 99) $grade_ = "A";
					                    if ($total <= 80 || $total <= 84) $grade_ = "A-";
					                    if ($total <= 75 || $total <= 79) $grade_ = "B+";
					                    if ($total <= 70 || $total <= 74) $grade_ = "B";
					                    if ($total <= 65 || $total <= 69) $grade_ = "B-";
					                    if ($total <= 60 || $total <= 64) $grade_ = "C+";
					                    if ($total <= 55 || $total <= 59) $grade_ = "C";
					                    if ($total <= 55 || $total <= 54) $grade_ = "C-";
					                    if ($total <= 40 || $total <= 49) $grade_ = "D";

					                    if ($grade_     == "A")  $mutu = 4.00;
					                    elseif ($grade_ == "A-") $mutu = 3.70;
					                    elseif ($grade_ == "B+") $mutu = 3.30;
					                    elseif ($grade_ == "B")  $mutu = 3.00;
					                    elseif ($grade_ == "B-") $mutu = 2.70;
					                    elseif ($grade_ == "C+") $mutu = 2.30;
					                    elseif ($grade_ == "C")  $mutu = 2.00;
					                    elseif ($grade_ == "C-") $mutu = 1.70;
					                    elseif ($grade_ == "D")  $mutu = 1.00;
					                    else $mutu     = 0.00;
					                    
					                    $total = $course->sks * $mutu;
					                    @endphp
					                    <tr>
					                        <td align="center" width="3%">{{ $no++ }}</td>
					                        <td>{{ $course->kode_mk }}</td>
					                        <td width="30%">{{ $course->nama_mk }}</td>
					                        <td align="center">{{ $course->sks }}</td>
					                        <td align="center">{{ $course->semester }}</td>
					                        <td align="center">{{ $grade_ }}</td>
					                        <td align="center">{{ number_format($mutu, 2) }}</td>
					                        <td align="center">{{ number_format($total, 2) }}</td>
					                    </tr>

					                    @php  
					                        $jml_krs  += $course->sks;
					                        $jml_mutu  = $jml_mutu + $total;
					                        $ipk       = $jml_mutu / $jml_krs;
					                    @endphp
					                    @endforeach
					                </tbody>
					                <tr>
					                	<td colspan="3"></td>
					                	<td align="center"><b>{{ $jml_krs }}</b></td>
					                	<td colspan="3"></td>
					                	<td align="center"><b>{{ number_format($jml_mutu, 2) }}</b></td>
					                </tr>
								</table>

								<table width="95%" style="margin-left: 5%;">
									<tr class="small">
										<td>Keterangan:</td>
										<td></td>
										<td>Cirebon, 31 Oktober 2014</td>
									</tr>
									<tr>
										<td width="15%" class="font-weight-bold">- IP Semester : {{ number_format($ipk, 2) }}</td>
										<td width="60%"></td>
										<td></td>
									</tr>
									<tr class="small">
										<td colspan="3" style="visibility: hidden;">$2y$10$v38CO6FaWHNWk1WUht2TaeDHENpfQ2o1042QhdFKFzdxjkYqvyLge</td>
									</tr>
									<tr>
										<td colspan="3" style="visibility: hidden;">$2y$10$v38CO6FaWHNWk1WUht2TaeDHENpfQ2o1042QhdFKFzdxjkYqvyLge</td>
									</tr>
									<tr class="medium">
										<td colspan="2"></td>
										<td class="font-weight-bold">Yucina Wilandari, M.Si.</td>
									</tr>
									<tr class="medium">
										<td colspan="2"></td>
										<td>NIP. 1970052347878</td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>