@extends('layouts._students.master')

@push('css')
    <style>
        .table-name tr td {
            font-weight: bold;
        }
        .table tbody td{
            border-top: 1px solid #a0a0a0;
        }
    </style>
@endpush

@section('content')
<div class="page-inner mt-2">
    <div class="col-12">
        <form action="{{ route('khs.store') }}" method="post">
        @csrf
        @card
            @slot('title')
            <strong>Kartu Hasil Studi</strong>
            @endslot
            

            <div class="form-group row">
                <label for="nim" class="col-sm-2 col-form-label">NIM</label>
                <div class="col-sm-4">
                    <input class="form-control form-control-sm" id="nim" name="nim" value="{{ $student->nim }}" readonly>
                </div>
            </div>
            <div class="form-group row">
                <label for="nama" class="col-sm-2 col-form-label">Nama</label>
                <div class="col-sm-6">
                    <input class="form-control form-control-sm" id="nama" name="nama" value="{{ $student->nama_mahasiswa }}" readonly>
                </div>
            </div>
            <div class="form-group row">
                <label for="kode_jurusan" class="col-sm-2 col-form-label">Jurusan</label>
                <div class="col-sm-4">
                    <input class="form-control form-control-sm" value="{{ $student->nama_jurusan }}" readonly>
                    <input class="form-control d-none" id="kode_jurusan" name="kode_jurusan" value="{{ $student->kode_jurusan }}" readonly>
                </div>
            </div>
            <div class="form-group row">
                <label for="semester" class="col-sm-2 col-form-label">Semester</label>
                <div class="col-sm-4">
                    <select name="semester" id="semester" class="form-control form-control-sm">
                        @for ($i = 1; $i <= 8; $i++)    
                            <option value="{{ $i }}" {{ $i == $student->semester ? 'selected' : '' }}>{{ $i }}</option>
                        @endfor
                    </select>
                </div>
            </div>

            <div class="row mb-3">
                <div class="col-sm-6 offset-sm-2">
                    <button class="btn btn-default btn-sm ml-2"> <i class="far fa-eye"></i> Lihat KHS</button>
                </div>
            </div>
            <hr>
        </form>
            
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Kode Matkul</th>
                        <th>Mata Kuliah</th>
                        <th class="text-center">SKS</th>
                        <th class="text-center">Semester</th>        
                        <th class="text-center">Nilai</th>        
                        <th class="text-center">Bobot</th>        
                        <th class="text-center">Mutu</th>        
                    </tr>
                </thead>
                
                <tbody>
                    @php 
                        $no = 1; 
                        $jml_krs  = 0;
                        $jml_mutu = 0;
                    @endphp

                    @foreach ($courses as $course)
                    
                    <!-- Logic PHP -->
                    @php
                    $total      = $course->nilaiakhir;

                    if ($total >= 85 || $total <= 99) $grade_ = "A";
                    if ($total <= 80 || $total <= 84) $grade_ = "A-";
                    if ($total <= 75 || $total <= 79) $grade_ = "B+";
                    if ($total <= 70 || $total <= 74) $grade_ = "B";
                    if ($total <= 65 || $total <= 69) $grade_ = "B-";
                    if ($total <= 60 || $total <= 64) $grade_ = "C+";
                    if ($total <= 55 || $total <= 59) $grade_ = "C";
                    if ($total <= 55 || $total <= 54) $grade_ = "C-";
                    if ($total <= 40 || $total <= 49) $grade_ = "D";

                    if ($grade_     == "A")  $mutu = 4.00;
                    elseif ($grade_ == "A-") $mutu = 3.70;
                    elseif ($grade_ == "B+") $mutu = 3.30;
                    elseif ($grade_ == "B")  $mutu = 3.00;
                    elseif ($grade_ == "B-") $mutu = 2.70;
                    elseif ($grade_ == "C+") $mutu = 2.30;
                    elseif ($grade_ == "C")  $mutu = 2.00;
                    elseif ($grade_ == "C-") $mutu = 1.70;
                    elseif ($grade_ == "D")  $mutu = 1.00;
                    else $mutu     = 0.00;
                    
                    $total = $course->sks * $mutu;
                    @endphp
                    <tr>
                        <td>{{ $no++ }}</td>
                        <td>{{ $course->kode_mk }}</td>
                        <td width="30%">{{ $course->nama_mk }}</td>
                        <td align="center">{{ $course->sks }}</td>
                        <td align="center">{{ $course->semester }}</td>
                        <td align="center">{{ $grade_ }}</td>
                        <td align="center">{{ number_format($mutu, 2) }}</td>
                        <td align="center">{{ number_format($total, 2) }}</td>
                    </tr>

                    @php  
                        $jml_krs  += $course->sks;
                        $jml_mutu  = $jml_mutu + $total;
                        $ipk       = $jml_mutu / $jml_krs;
                    @endphp
                    @endforeach
                    
                </tbody>
                <tr>
                    <td style="text-align: right; font-weight: bold;" colspan="3">Total SKS</td>
                    <td style="font-size: px; text-align: right"><b>{{ $jml_krs }}</b></td>
                    <td colspan="4"></td>
                </tr>
                <tr>
                    <td style="text-align: right; font-weight: bold;" colspan="3">Total Mutu</td>
                    <td style="font-size: px; text-align: right;"><b>{{ number_format($jml_mutu, 2) }}</b></td>
                    <td colspan="4"></td>
                </tr>
                <tr>
                    <td style="text-align: right; font-weight: bold;" colspan="3">IPS </td>
                    <td style="text-align: right;"><b>{{ number_format($ipk, 2) }}</b></td>
                    <td colspan="4"></td>
                </tr>
            </table>
            <div class="row card-footer mt-3">
                <div class="col-sm-6">
                    <a onclick="window.open('{{ url('/user/student/khs/detail/print') }}', 'Nota PDF', 'height=675, width=1024, left=175, scrollbars=yes');" class="btn btn-primary btn-sm text-white"> <i class="fas fa-print"></i> Cetak KHS</a>
                </div>                   
            </div>
        @endcard
    </div>
</div>
@endsection

@push('scripts')
<script>
    $('.table').DataTable({
        "language": {
            "zeroRecords": "<strong>Data tidak ditemukan</strong>"
        }
    })

    $('label.offset-sm-1').append(` <span class="required-label">*</span>`)
    $('.form-group .form-control').addClass('form-control-sm')
</script>
@endpush