@extends('layouts._students.master')

@push('css')
    <style>
        .form-check, .form-group {
            padding: 0;
        }
        .form-check label, .form-group label {
            font-weight: normal;
        }
    </style>
@endpush

@section('content')
<div class="row mt-2 d-lg-block d-none"></div>
<div class="page-inner">
    <div class="row">
        <div class="col-12">
            @card
                @slot('title')
                    Keluarga {{ ucfirst(Auth::user()->name) }}
                @endslot
                
                <form action="#" method="post">
                    @csrf 
                    <div class="form-group row">
                        <label for="kk" class="col-sm-4 col-form-label">No Kartu Keluarga <small>(KK)</small></label>
                        <div class="col-sm-6">
                            <input class="form-control" name="kk" id="kk">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="ktp_ayah" class="col-sm-4 col-form-label">No KTP Ayah</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="ktp_ayah" id="ktp_ayah">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nama_ayah" class="col-sm-4 col-form-label">Nama Ayah</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="nama_ayah" id="nama_ayah">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="telp_ayah" class="col-sm-4 col-form-label">Telpon Ayah</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="telp_ayah" id="telp_ayah">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="tgl_lahir_ayah" class="col-sm-4 col-form-label">Tanggal Lahir Ayah</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="tgl_lahir_ayah" id="tgl_lahir_ayah">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="jenis_pekerjaan_ayah" class="col-sm-4 col-form-label">Jenis Pekerjaan Ayah</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="jenis_pekerjaan_ayah" id="jenis_pekerjaan_ayah">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="penghasilan_ayah" class="col-sm-4 col-form-label">Rata-rata penghasilan Ayah</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="penghasilan_ayah" id="penghasilan_ayah">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="pendidikan_ayah" class="col-sm-4 col-form-label">Jenjang Pendidikan Ayah</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="pendidikan_ayah" id="pendidikan_ayah">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="ktp_ibu" class="col-sm-4 col-form-label">No KTP Ibu</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="ktp_ibu" id="ktp_ibu">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nama_ibu" class="col-sm-4 col-form-label">Nama Ibu</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="nama_ibu" id="nama_ibu">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="telp_ibu" class="col-sm-4 col-form-label">Telpon Ibu</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="telp_ibu" id="telp_ibu">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="tgl_lahir_ibu" class="col-sm-4 col-form-label">Tanggal Lahir Ibu</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="tgl_lahir_ibu" id="tgl_lahir_ibu">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="jenis_pekerjaan_ibu" class="col-sm-4 col-form-label">Jenis Pekerjaan Ibu</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="jenis_pekerjaan_ibu" id="jenis_pekerjaan_ibu">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="penghasilan_ibu" class="col-sm-4 col-form-label">Rata-rata penghasilan Ibu</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="penghasilan_ibu" id="penghasilan_ibu">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="pendidikan_ibu" class="col-sm-4 col-form-label">Jenjang Pendidikan Ibu</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="pendidikan_ibu" id="pendidikan_ibu">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="jml_keluarga" class="col-sm-4 col-form-label">Jumlah anggota keluarga <small style="font-size: .8em;">(termasuk ayah & ibu jika masih ada)</small></label>
                        <div class="col-sm-6">
                            <input class="form-control" name="jml_keluarga" id="jml_keluarga">
                        </div>
                    </div>
                </form>
            @endcard
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    $('.col-form-label').addClass('offset-md-1')
    $('label.offset-md-1').append(` <span class="required-label">*</span>`)
    $('.form-group .form-control').addClass('form-control-sm')
</script>
@endpush