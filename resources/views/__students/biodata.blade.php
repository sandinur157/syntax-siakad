@extends('layouts._students.master')

@push('css')
    <style>
        .form-check, .form-group {
            padding: 0;
        }
        .form-check label, .form-group label {
            font-weight: normal;
        }
    </style>
@endpush

@section('content')
<div class="row mt-2 d-lg-block d-none"></div>
<div class="page-inner">
    <div class="row">
        <div class="col-12">
            @card
                @slot('title')
                    Biodata {{ ucfirst(Auth::user()->name) }}
                @endslot
                
                <form action="#" method="post">
                    @csrf 
                    <div class="form-group row">
                        <label for="nim" class="col-sm-4 col-form-label">NIM</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="nim" id="nim">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="prodi" class="col-sm-4 col-form-label">Prodi</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="prodi" id="prodi">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nama" class="col-sm-4 col-form-label">Nama</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="nama" id="nama">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="ttl" class="col-sm-4 col-form-label">Tempat / Tanggal Lahir</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="ttl" id="ttl">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="dosen_p" class="col-sm-4 col-form-label">Dosen Pembimbing Akademik</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="dosen_p" id="dosen_p">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="jk" class="col-sm-4 col-form-label">Jenis Kelamin</label>
                        <div class="custom-control custom-radio">
                            <input type="radio" id="laki-laki" name="jenis_kelamin" class="custom-control-input" value="L">
                            <label class="custom-control-label" for="laki-laki">Laki-laki</label>
                        </div>
                        <div class="custom-control custom-radio">
                            <input type="radio" id="Perempuan" name="jenis_kelamin" class="custom-control-input" value="P">
                            <label class="custom-control-label" for="Perempuan">Perempuan</label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="bahasa" class="col-sm-4 col-form-label">Bahasa</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="bahasa" id="bahasa">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nisn" class="col-sm-4 col-form-label">No Induk Sekolah Nasional</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="nisn" id="nisn">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nirm" class="col-sm-4 col-form-label">No Induk Registrasi Mahasiswa</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="nirm" id="nirm">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="npwp" class="col-sm-4 col-form-label">NPWP</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="npwp" id="npwp">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="fakultas" class="col-sm-4 col-form-label">Fakultas</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="fakultas" id="fakultas">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="angkatan" class="col-sm-4 col-form-label">Angkatan</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="angkatan" id="angkatan">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="status" class="col-sm-4 col-form-label">Status</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="status" id="status">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="email" class="col-sm-4 col-form-label">Alamat Email</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="email" id="email">
                        </div>
                    </div>
                    <div class="form-group row">
                        <!-- MA Negeri -->
                        <label for="pend_s" class="col-sm-4 col-form-label">Asal Pendidikan Sebelumnya</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="pend_s" id="pend_s">
                        </div>
                    </div>
                    <div class="form-group row">
                        <!-- Name school -->
                        <label for="pend_sb" class="col-sm-4 col-form-label">ASAL PENDIDIKAN SEBELUMNYA</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="pend_sb" id="pend_sb">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="npsn" class="col-sm-4 col-form-label">NPSN Pendidikan Sebelumnya</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="npsn" id="npsn">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nama_ijazah" class="col-sm-4 col-form-label">Nama yang tertera di Ijazah SMA / Sederajat</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="nama_ijazah" id="nama_ijazah">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="no_ijazah" class="col-sm-4 col-form-label">No Ijazah Pendidikan Sebelumnya</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="no_ijazah" id="no_ijazah">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="alamat_pend_s" class="col-sm-4 col-form-label">Alamat Pendidikan Sebelumnya</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="alamat_pend_s" id="alamat_pend_s">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="asal_smp" class="col-sm-4 col-form-label">Asal SMP / Sederajat</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="asal_smp" id="asal_smp">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="alamat_asal_smp" class="col-sm-4 col-form-label">Alamat Asal SMP / Sederajat</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="alamat_asal_smp" id="alamat_asal_smp">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="asal_sd" class="col-sm-4 col-form-label">Asal SD</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="asal_sd" id="asal_sd">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="alamat_asal_sd" class="col-sm-4 col-form-label">Alamat Asal SD</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="alamat_asal_sd" id="alamat_asal_sd">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="paud" class="col-sm-4 col-form-label">Pernah PAUD (Pendidikan Anak Usia Dini)</label>
                        <div class="col-sm-6">
                            <input type="checkbox" name="paud" id="paud">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="tk" class="col-sm-4 col-form-label">Pernah TK (Taman Kanak-kanak)</label>
                        <div class="col-sm-6">
                            <input type="checkbox" name="tk" id="tk">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="tt_kuliah" class="col-sm-4 col-form-label">Tempat Tinggal Saat Kuliah</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="tt_kuliah" id="tt_kuliah">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="ukuran_jaket" class="col-sm-4 col-form-label">Ukuran Jaket</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="ukuran_jaket" id="ukuran_jaket">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="menetap_luar_negeri" class="col-sm-4 col-form-label">Pernah Menetap di Luar Negeri</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="menetap_luar_negeri" id="menetap_luar_negeri">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="tiggi_badan" class="col-sm-4 col-form-label">Tiggi Badan (cm)</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="tiggi_badan" id="tiggi_badan">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="berat_badan" class="col-sm-4 col-form-label">Berat Badan (kg)</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="berat_badan" id="berat_badan">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="no_hp" class="col-sm-4 col-form-label">No HP</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="no_hp" name="no_hp">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="no_sim" class="col-sm-4 col-form-label">No Surat Ijin Mengemudi</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="no_sim" name="no_sim">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="transport_kuliah" class="col-sm-4 col-form-label">Transport Mahasiswa Saat Kuliah</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="transport_kuliah" name="transport_kuliah">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="organisasi" class="col-sm-4 col-form-label">Organisasi Intra Kampus yang diikuti</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="organisasi" name="organisasi">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nama_organisasi" class="col-sm-4 col-form-label">Nama Organisasi Mahasiswa</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="nama_organisasi" name="nama_organisasi">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="surat_organisasi" class="col-sm-4 col-form-label">Surat Penunjukan Sebagai Pengurus Organisasi <small style="font-size: .8em;">(Jika Ada)</small></label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="surat_organisasi" name="surat_organisasi">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="hobi" class="col-sm-4 col-form-label">Hobi</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="hobi" id="hobi">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="minat_seni" class="col-sm-4 col-form-label">Minat Seni</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="minat_seni" id="minat_seni">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="bahasa_1" class="col-sm-4 col-form-label">Kemampuan Bahasa 1 / Utama</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="bahasa_1" id="bahasa_1">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="bahasa_2" class="col-sm-4 col-form-label">Kemampuan Bahasa 2</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="bahasa_2" id="bahasa_2">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="bahasa_3" class="col-sm-4 col-form-label">Kemampuan Bahasa 3</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="bahasa_3" id="bahasa_3">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="gol_darah" class="col-sm-4 col-form-label">Golongan Darah</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="gol_darah" id="gol_darah">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="status_nikah" class="col-sm-4 col-form-label">Status Nikah</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="status_nikah" id="status_nikah">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="agama" class="col-sm-4 col-form-label">Agama</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="agama" id="agama">
                        </div>
                    </div>
                </form>
            @endcard
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    $('.col-form-label').addClass('offset-md-1')
    $('label.offset-md-1').append(` <span class="required-label">*</span>`)
    $('.form-group .form-control').addClass('form-control-sm')
</script>
@endpush