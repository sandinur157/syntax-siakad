@extends('layouts._students.master')

@section('content')
<div class="page-inner mt-2">
    <div class="col-12">
        @card
            @slot('title')
            Upload & Download Lampiran
            @endslot
            
            <form method="post" data-toggle="validator" action="#" enctype="multipart/form-data">
                <div class="row justify-content-center">
                    <div class="col-4">
                        <img src="{{ asset('/assets/img/uploads/') }}/{{ Auth::user()->image }}" alt="..." class="rounded img-fluid" width="80%">
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col">
                        <p><i class="fas fa-file-upload"></i> <small>Upload Ijazah / Surat Keterangan Lulus (Pendidikan Sebelumnya) * (Maks 3 Mb)</small></p>
                        <p><i class="fas fa-file-upload"></i> <small>Upload Transkrip Nilai Lulus (Pendidikan Sebelumnya) * (Maks 3 Mb)</small></p>
                        <p><i class="fas fa-file-upload"></i> <small>Upload KTP / Kartu Pelajar (Pendidikan Sebelumnya) / Kartu Identitas Lain * (Maks 3 Mb)</small></p>
                    </div>
                </div>
                <div class="row">
                    <form action="" class="dropzone">
                        <div class="dz-message" data-dz-message>
                            <div class="icon">
                                <i class="flaticon-file"></i>
                            </div>
                            <h4 class="message">Drag and Drop files here</h4>
                            <div class="note">(This is just a demo dropzone. Selected files are <strong>not</strong> actually uploaded.)</div>
                        </div>
                        <div class="fallback">
                            <input name="file" type="file" multiple />
                        </div>
                    </form>
                </div>
            </form>
        @endcard
    </div>
</div>
@endsection

@push('scripts')
<script>
    $('.profile').parent().addClass('active');
    
    $('label.col-sm-2').addClass('offset-sm-1')
    $('label.offset-sm-1').append(` <span class="required-label">*</span>`)
    $('.form-group .form-control').addClass('form-control-sm')

    $(".dropzone").dropzone();
</script>
@endpush