<table class="table">
	<thead>
		{{ $thead }}
	</thead>

	<tbody>
		{{ $slot }}		    
	</tbody>
</table>