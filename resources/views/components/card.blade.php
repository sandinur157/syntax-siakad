<div class="card">
	<div class="card-header">
		<div class="card-title">{{ $title }}</div>
    </div>
	<div class="card-body">
		<div class="card-category mb-3"></div>
		{{ $slot }}
	</div>
	
</div>