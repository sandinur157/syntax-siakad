@extends('layouts.master')

@section('content')
<div class="panel-header bg-primary-gradient">
    <div class="page-inner py-5">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
            <div>
                <h2 class="text-white pb-2 fw-bold">Jadwal Perkuliahan</h2>
            </div>
            <div class="ml-md-auto py-2 py-md-0">
                <button class="btn btn-sm btn-border border-white text-white" disabled style="cursor: not-allowed;"><i class="fas fa-plus-circle"></i> Tambah</button>
            </div>
        </div>
    </div>
</div>

<div class="page-inner mt--5">
    <div class="row mt--2">
        <div class="col-12">
            @card
                @slot('title')
                    
                @endslot

                @table
                    @slot('thead')
                        <th>No</th>
                        <th>Mata Kuliah</th>
                        <th>Kelas</th>
                        <th>Jurusan</th>
                        <th>Semester</th>                    
                        <th>Ruang</th>
                        <th>Hari</th>                   
                        <th>Jam</th>
                        <th>Dosen</th>
                        <th>Aksi</th>
                    @endslot
                    
                    @php $no = 1; @endphp
                    @foreach ($schedules as $schedule)
                        @foreach ($lecturers as $lecturer)
                            @if (Auth::user()->user_id == $lecturer->nidn && $lecturer->kode == $schedule->kode_dosen)
                                <tr>
                                    <td width="5%">{{ $no++ }}.</td>
                                    <td>
                                        @foreach ($courses as $course)
                                            {{ $course->kode == $schedule->kode_mk ? $course->nama : ''}}
                                        @endforeach
                                    </td>
                                    <td>
                                        @foreach ($class_times as $time)
                                            {{ $time->id == $schedule->kelas_id ? $time->nama : ''}}
                                        @endforeach
                                    </td>
                                    <td>
                                        @foreach ($departments as $department)
                                            {{ $department->kode == $schedule->kode_jurusan ? $department->nama : ''}}
                                        @endforeach
                                    </td>
                                    <td>{{ $schedule->semester }}</td>
                                    <td>
                                        @foreach ($rooms as $room)
                                            {{ $room->kode == $schedule->kode_ruang ? $room->nama : ''}}
                                        @endforeach
                                    </td>
                                    <td>{{ $schedule->hari }}</td>
                                    <td>{{ $schedule->jam_mulai .' - '. $schedule->jam_selesai }}</td>
                                    <td>
                                        @foreach ($lecturers as $lecturer)
                                            {{ $lecturer->kode == $schedule->kode_dosen ? $lecturer->nama : ''}}
                                        @endforeach
                                    </td>
                                    <td width="5%">
                                        <a href="{{ route('schedules.edit', $schedule->id) }}" class="btn btn-link btn-primary" title="Edit Nilai"><i class="far fa-edit"></i></a>
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                    @endforeach
                @endtable
            @endcard
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    $('.table').DataTable({
        "language": {
            "zeroRecords": "<strong>Data tidak ditemukan</strong>"
        }
    })

    $('.card-header').addClass('d-none')
</script>
@endpush