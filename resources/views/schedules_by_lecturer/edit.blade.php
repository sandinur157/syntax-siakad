@extends('layouts.master')

@section('content')
<div class="page-inner mt-2">
    <div class="col-12">
        @card
            @slot('title')
                Ubah Jadwal
            @endslot
            
            <form method="post" action="{{ route('schedules.update', $schedule->id) }}">
                @csrf @method('put')
                <div class="form-group row">
                    <label for="matkul" class="col-sm-2 col-form-label">Mata Kuliah</label>
                    <div class="col-sm-4">
                        <input type="text" name="kode_mk" id="matkul" readonly class="form-control" value="{{ $schedule->kode_mk }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="kelas" class="col-sm-2 col-form-label">Kelas</label>
                    <div class="col-sm-4">
                        <input type="text" name="kelas_id" id="kelas" readonly class="form-control d-none" value="{{ $schedule->kelas_id }}">
                        @foreach ($class_times as $ct)
                        @if ($ct->id == $schedule->kelas_id)
                            <input type="text" id="kelas" readonly class="form-control" value="{{ $ct->nama }}">
                        @endif
                        @endforeach
                    </div>
                </div>
                <div class="form-group row">
                    <label for="presensi" class="col-sm-2 col-form-label">Presensi</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="presensi" id="presensi" required value="0.15">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="tugas" class="col-sm-2 col-form-label">Tugas</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="tugas" id="tugas" required value="0.1">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="quiz" class="col-sm-2 col-form-label">Quiz</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="quiz" id="quiz" required value="0.05">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="uts" class="col-sm-2 col-form-label">UTS</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="uts" id="uts" required value="0.3">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="uas" class="col-sm-2 col-form-label">UAS</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="uas" id="uas" required value="0.4">
                    </div>
                </div>

                <div class="card-footer">
                    <div class="row">
                        <div class="col-sm-6 offset-sm-3">
                            <button class="btn btn-primary"> <i class="far fa-save"></i> Simpan</button>
                            <a href="{{ url('/schedules') }}" class="btn btn-danger ml-3"><i class="fas fa-arrow-circle-left"></i> Kembali</a>
                        </div>                                      
                    </div>
                </div>
            </form>
        @endcard
    </div>
</div>
@endsection

@push('scripts')
<script>
    $('label.col-sm-2').addClass('offset-sm-1')
    $('label.offset-sm-1').append(` <span class="required-label">*</span>`)
    $('.form-group .form-control').addClass('form-control-sm')
</script>
@endpush