<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Department;
use App\Student;
use App\KRS;
use DB;

class KRSController extends Controller
{

    public function index()
    {
        $student = Student::join('departments', 'departments.kode', 'students.kode_jurusan')
                ->join('class_times', 'class_times.id', 'students.kelas_id')
                ->select('students.nama as nama_mahasiswa', 'students.nim', 'departments.nama as nama_jurusan', 'departments.kode as kode_jurusan', 'class_times.nama as nama_kelas', 'class_times.id as kelas_id', 'students.semester', 'students.angkatan')
                ->where('nim', \Auth::user()->user_id)
                ->first();

        $tahun_akademik = ($student->angkatan -1).'/'.$student->angkatan;

        $courses = DB::select("
                SELECT krs.nim, krs.kode_mk, krs.kode_jurusan, krs.id as id_krs,
                       schedules.kode_mk, schedules.kode_ruang, schedules.kode_dosen, schedules.kelas_id, schedules.jam_mulai, schedules.jam_selesai, schedules.created_at AS tanggal, schedules.hari,
                       courses.kode AS kode_mk, courses.nama AS nama_mk, courses.semester, courses.sks,
                       rooms.kode as kode_ruang, rooms.nama as nama_ruang,
                       lecturers.kode as kode_dosen, lecturers.nama as nama_dosen,
                       class_times.id as kelas_id, class_times.nama as nama_kelas,
                       departments.kode as kode_jurusan, departments.nama as nama_jurusan
                FROM   krs, courses, schedules, lecturers, rooms, class_times, departments
                WHERE  krs.kode_mk  = schedules.kode_mk AND
                       courses.kode = schedules.kode_mk AND
                       schedules.kode_dosen = lecturers.kode AND
                       schedules.kode_ruang = rooms.kode AND
                       schedules.kelas_id   = class_times.id AND
                       krs.status_krs       = 0 AND
                       krs.kode_jurusan     = departments.kode AND
                       krs.nim              = '$student->nim'
            ");

        $count = count($courses);

        return view('krs.index', compact('student', 'courses', 'count'));
    }

    public function store(Request $request)
    {
    	// 
    }

    public function detail()
    {
    	$student = Student::join('departments', 'departments.kode', 'students.kode_jurusan')
                ->join('class_times', 'class_times.id', 'students.kelas_id')
                ->select('students.nama as nama_mahasiswa', 'students.nim', 'departments.nama as nama_jurusan', 'departments.kode as kode_jurusan', 'class_times.nama as nama_kelas', 'students.semester', 'students.angkatan')
                ->where('nim', \Auth::user()->user_id)
                ->first();

        $courses = DB::select("
                SELECT krs.nim, krs.kode_mk, krs.kode_jurusan,
                       schedules.kode_mk, schedules.kode_ruang, schedules.kode_dosen, schedules.kelas_id, schedules.jam_mulai, schedules.jam_selesai, schedules.created_at AS tanggal, schedules.hari,
                       courses.kode AS kode_mk, courses.nama AS nama_mk, courses.semester, courses.sks,
                       rooms.kode as kode_ruang, rooms.nama as nama_ruang,
                       lecturers.kode as kode_dosen, lecturers.nama as nama_dosen,
                       class_times.id as kelas_id, class_times.nama as nama_kelas
                FROM   krs, courses, schedules, lecturers, rooms, class_times
                WHERE  krs.kode_mk  = schedules.kode_mk AND
                       courses.kode = schedules.kode_mk AND
                       schedules.kode_dosen = lecturers.kode AND
                       schedules.kode_ruang = rooms.kode AND
                       schedules.kelas_id   = class_times.id AND
                       krs.status_krs       = 1 AND
                       krs.nim              = '$student->nim'
            ");

        return view('krs.detail', compact('student', 'courses'));
    }

    public function print()
    {
        $student = Student::join('departments', 'departments.kode', 'students.kode_jurusan')
                ->join('class_times', 'class_times.id', 'students.kelas_id')
                ->select('students.nama as nama_mahasiswa', 'students.nim', 'departments.nama as nama_jurusan', 'departments.kode as kode_jurusan', 'class_times.nama as nama_kelas', 'students.semester', 'students.angkatan')
                ->where('nim', \Auth::user()->user_id)
                ->first();

        $courses = DB::select("
                SELECT krs.nim, krs.kode_mk, krs.kode_jurusan, krs.ket,
                       schedules.kode_mk, schedules.kode_ruang, schedules.kode_dosen, schedules.kelas_id, schedules.jam_mulai, schedules.jam_selesai, schedules.created_at AS tanggal, schedules.hari,
                       courses.kode AS kode_mk, courses.nama AS nama_mk, courses.semester, courses.sks,
                       rooms.kode as kode_ruang, rooms.nama as nama_ruang,
                       lecturers.kode as kode_dosen, lecturers.nama as nama_dosen,
                       class_times.id as kelas_id, class_times.nama as nama_kelas
                FROM   krs, courses, schedules, lecturers, rooms, class_times
                WHERE  krs.kode_mk  = schedules.kode_mk AND
                       courses.kode = schedules.kode_mk AND
                       schedules.kode_dosen = lecturers.kode AND
                       schedules.kode_ruang = rooms.kode AND
                       schedules.kelas_id   = class_times.id AND
                       krs.status_nilai     = 0 AND
                       krs.nim              = '$student->nim'
            ");

    	return view('krs.print', compact('student', 'courses'));
    }

    public function update_krs(Request $request)
    {

        foreach ($request->pilih as $list) {
            $krs = KRS::where('kode_mk', $list);

            $krs->update([
                'ket'        => 'Baru',
                'status_krs' => 1
            ]);
        }

        $student = Student::where('nim', $request->nim)->first();
        $student->update([
            'status_krs' => 0,
            'krs' => 0
        ]);

        return redirect(url('/user/student/krs/detail'));
    }
}
