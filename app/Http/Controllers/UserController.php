<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use Hash;
use Auth;

class UserController extends Controller
{
    public function index()
    {
        $users = User::latest()->get();
        $roles = Role::all();
        return view('users.index', compact('users', 'roles'));
    }
    
    public function edit($id)
    {
        $user = User::find($id);
        return view('users.edit', compact('user'));
    }

    public function edit_password()
    {
        $user = User::find(Auth::user()->id);
        if (\Auth::user()->role_id == 3) {
            return view('__students.edit_password', compact('user'));
        } else return view('users.edit', compact('user'));
    }
    
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        if(!empty($request['password'])) $user->password = bcrypt($request['password']);
        $user->update();

        if(Auth::user()->role_id == 3) : 
            return redirect('/user/student/profile');
        elseif(Auth::user()->role_id == 4) :
            return redirect('/user/lecturer/profile');
        else :
            return redirect('/users');
        endif;
    }

    public function profile() 
    {
    	$user = Auth::user();
    	return view('users.profile', compact('user'));
    }

    public function change_profile(Request $request, $id)
    {
        $foto = Auth::user()->image;
        $password = Auth::user()->password;
        
        if ($request->hasFile('foto')) $foto = $this->saveFile($request->nama, $request->file('foto'));
        if(!empty($request['password'])) $password = bcrypt($request['password']);

        $user = Auth::user()->find($id)->update([
            'name' => $request->nama,
            'password' => $password,
            'image' => $foto,
        ]);

        return redirect('/');
    }

    private function saveFile($name, $photo)
    {
        $images = str_slug($name) . time() .'.' . $photo->getClientOriginalExtension();
        $photo->move(public_path('assets/img/uploads'), $images);
        return $images;
    }
}
