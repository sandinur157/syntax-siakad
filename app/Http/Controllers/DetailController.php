<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DetailController extends Controller
{
    public function biodata()
    {
    	return view('__students.biodata');
    }

    public function address()
    {
    	return view('__students.address');
    }

    public function family()
    {
    	return view('__students.family');
    }

    public function upload()
    {
        return view('__students.upload');
    }

    public function guardian()
    {
    	
    }
}
