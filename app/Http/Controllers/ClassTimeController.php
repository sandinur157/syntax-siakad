<?php

namespace App\Http\Controllers;

use App\Class_time;
use Illuminate\Http\Request;

class ClassTimeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Class_time  $class_time
     * @return \Illuminate\Http\Response
     */
    public function show(Class_time $class_time)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Class_time  $class_time
     * @return \Illuminate\Http\Response
     */
    public function edit(Class_time $class_time)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Class_time  $class_time
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Class_time $class_time)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Class_time  $class_time
     * @return \Illuminate\Http\Response
     */
    public function destroy(Class_time $class_time)
    {
        //
    }
}
