<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\User_menu;
use App\User_sub_menu;
use App\User_access_menu;
use App\Student;
use App\Lecturer;
use App\Course;
use App\Department;
use App;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $students = Student::all()->count();
        $lecturers = Lecturer::all()->count();
        $courses = Course::all()->count();
        $departments = Department::all()->count();

        if(Auth::user()->role_id == 1 || Auth::user()->role_id == 2) :
            return view('home.administrator', compact('students', 'lecturers', 'courses', 'departments'));
        elseif(Auth::user()->role_id == 3) :
            return view('home.students');
        elseif(Auth::user()->role_id == 4) :
            return view('home.lecturers');
        endif;
    }

}
