<?php

namespace App\Http\Controllers;

use App\Lecturer;
use Illuminate\Http\Request;
use App\Department;
use File;

class LecturerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lecturers = Lecturer::latest()->get();
        return view('lecturers.index', compact('lecturers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('lecturers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $foto = null;
        if ($request->hasFile('foto')) {
            $foto = $this->saveFile($request->nama, $request->file('foto'));
        }

        $lecturers = Lecturer::create([
            'kode' => $request->kode,
            'nidn' => $request->nidn,
            'nama' => $request->nama,
            'pend_s1' => $request->pend_s1,
            'pend_s2' => $request->pend_s2,
            'pend_s3' => $request->pend_s3,
            'alamat' => $request->alamat,
            'telpon' => $request->telpon,
            'email' => $request->email,
            'foto' => $foto,
        ]);

        $user = \Auth::user()->create([
            'name' => $request->nama,
            'email' => $request->email,
            'user_id' => $request->nidn,
            'password' => bcrypt($request['password']),
            'image' => $foto,
            'role_id' => 4
        ]);

        return redirect(url('/lecturers'));
    }

    private function saveFile($name, $photo)
    {
        $images = str_slug($name) . time() .'.' . $photo->getClientOriginalExtension();
        $photo->move(public_path('assets/img/uploads'), $images);
        return $images;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show(Lecturer $lecturer)
    {
        return view('lecturers.detail', compact('lecturer'));
    }

    public function detail()
    {
        $lecturer = Lecturer::where('nidn', \Auth::user()->user_id)->first();
        $departments = Department::all();
        return view('lecturers.detail', compact('lecturer', 'departments'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Lecturer $lecturer)
    {
        return view('lecturers.edit', compact('lecturer'));
    }

    public function edit_profile()
    {
        $lecturer = Lecturer::where('nidn', \Auth::user()->user_id)->first();
        $departments = Department::all();
        return view('lecturers.edit', compact('lecturer', 'departments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Lecturer $lecturer)
    {
        $foto = $lecturer->foto;
        if ($request->hasFile('foto')) {
            !empty($foto) ? File::delete(public_path('assets/img/uploads/' . $foto)) : null;
            $foto = $this->saveFile($request->nama, $request->file('foto'));
        }

        $lecturer->update([
            'kode' => $request->kode,
            'nidn' => $request->nidn,
            'nama' => $request->nama,
            'pend_s1' => $request->pend_s1,
            'pend_s2' => $request->pend_s2,
            'pend_s3' => $request->pend_s3,
            'alamat' => $request->alamat,
            'telpon' => $request->telpon,
            'email' => $request->email,
            'foto' => $foto,
        ]);

        $user = \Auth::user()->where('user_id', $lecturer->nidn)->update([
            'name' => $request->nama,
            'image' => $foto,
        ]);

        if(\Auth::user()->role_id == 4) return redirect(url('/user/lecturer/profile'));
        else return redirect(url('/lecturers'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Lecturer $lecturer)
    {
        $lecturer->delete();
        return redirect()->back();
    }
}
