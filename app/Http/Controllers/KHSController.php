<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KHS;
use App\Student;
use App\Department;
use DB;

class KHSController extends Controller
{
    public function index()
    {
        $departments = Department::all();
    	$student = Student::join('departments', 'departments.kode', 'students.kode_jurusan')
                ->join('class_times', 'class_times.id', 'students.kelas_id')
                ->select('students.nama as nama_mahasiswa', 'students.nim', 'departments.nama as nama_jurusan', 'departments.kode as kode_jurusan', 'class_times.nama as nama_kelas', 'students.semester', 'students.angkatan')
                ->where('nim', \Auth::user()->user_id)
                ->first();



        $courses = DB::select("
                    SELECT 
                           students.nim, students.khs, students.kode_jurusan,
                           courses.kode AS kode_mk, courses.nama AS nama_mk, courses.semester, courses.sks,
                           grades.*
                    FROM 
                           grades, courses, students
                    WHERE
							grades.kode_mk  = courses.kode AND
							grades.nim      = students.nim AND
							students.khs    = 1 AND
							grades.semester = '$student->semester' AND
							grades.nim      = '$student->nim'
                           
                    ");

        return view('khs.index', compact('student', 'departments', 'courses'));
    }

    public function store(Request $request)
    {
        $students = Student::where('nim', $request->nim);
        $students->update([
            'semester' => $request->semester
        ]);

        return redirect(url('user/student/khs'));
    }

    public function print()
    {
        $departments = Department::all();

        $student = Student::join('departments', 'departments.kode', 'students.kode_jurusan')
                ->join('class_times', 'class_times.id', 'students.kelas_id')
                ->select('students.nama as nama_mahasiswa', 'students.foto as foto_student', 'students.nim', 'departments.nama as nama_jurusan', 'departments.kode as kode_jurusan', 'class_times.nama as nama_kelas', 'students.semester', 'students.angkatan')
                ->where('nim', \Auth::user()->user_id)
                ->first();



        $courses = DB::select("
                    SELECT 
                           students.nim, students.khs, students.kode_jurusan, 
                           courses.kode AS kode_mk, courses.nama AS nama_mk, courses.semester, courses.sks,
                           grades.*
                    FROM 
                           grades, courses, students
                    WHERE
                            grades.kode_mk  = courses.kode AND
                            grades.nim      = students.nim AND
                            students.khs    = 1 AND
                            grades.semester = '$student->semester' AND
                            grades.nim      = '$student->nim'
                           
                    ");
        
        return view('khs.print', compact('student', 'courses'));
    }
}
