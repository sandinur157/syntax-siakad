<?php

namespace App\Http\Controllers;

use App\Grade;
use App\Schedule;
use App\Room;
use App\Course;
use App\Lecturer;
use App\KRS;
use DB;
use Illuminate\Http\Request;

class GradeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = DB::select("SELECT 
                            schedules.kode_mk, schedules.kode_ruang, schedules.kode_dosen, schedules.kelas_id, schedules.jam_mulai, schedules.jam_selesai, schedules.created_at AS tanggal,
                            lecturers.kode AS kode_dosen, lecturers.nama AS nama_dosen, lecturers.id as id_dosen, lecturers.nidn,
                            courses.kode AS kode_mk, courses.nama AS nama_mk, courses.semester, courses.sks,
                            rooms.kode AS kode_ruang, rooms.nama AS nama_ruang,
                            class_times.nama AS nama_kelas 
                        FROM 
                            schedules, lecturers, courses, rooms, class_times
                        WHERE 
                            schedules.kode_dosen = lecturers.kode AND
                            schedules.kode_mk    = courses.kode   AND    
                            schedules.kelas_id   = class_times.id AND
                            schedules.kode_ruang = rooms.kode");
        
        if (\Auth::user()->role_id == 1 || \Auth::user()->role_id == 2)
            return view('grades.index', compact('courses'));
        else return view('grades_by_lecturer.index', compact('courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $nilaiakhir = ($request->presensi * 100 / $request->presensi * $request->prepre) + ($request->tugas * $request->pretug) + ($request->quiz * $request->prequi) + ($request->uts * $request->preuts) + ($request->uas * $request->preuas);
        $presensi   = isset($request->presensi) ? $request->presensi : null;
        $quiz       = isset($request->quiz) ? $request->quiz : null;
        $prepre     = isset($request->prepre) ? $request->prepre : null;
        $pretug     = isset($request->pretug) ? $request->pretug : null;
        $prequi     = isset($request->prequi) ? $request->prequi : null;
        $preuts     = isset($request->preuts) ? $request->preuts : null;
        $preuas     = isset($request->preuas) ? $request->preuas : null;

        $grade = Grade::create([
            'nim'        => $request->nim,
            'kode_mk'    => $request->kode_mk,
            'kode_dosen' => $request->kode_dosen,
            'semester'   => $request->semester,
            'presensi'   => $presensi,
            'tugas'      => $request->tugas,
            'quiz'       => $quiz,
            'uts'        => $request->uts,
            'uas'        => $request->uas,
            'nilaiakhir' => $nilaiakhir,
            'ket'        => $request->ket,
            'kelas'      => $request->kelas,
            'prepre'     => $request->prepre,
            'pretug'     => $request->pretug,
            'pretug'     => $request->pretug,
            'prequi'     => $request->prequi,
            'preuts'     => $request->preuts,
            'preuas'     => $request->preuas
        ]);

        $krs = KRS::where('nim', $request->nim)->where('kode_mk', $request->kode_mk);
        $krs->update([
            'status_nilai' => 0
        ]);

        return redirect(url('/grades'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Grade  $grade
     * @return \Illuminate\Http\Response
     */
    public function show(Grade $grade)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Grade  $grade
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kode_mk = $_GET['kode_mk'];
        $course = DB::select("SELECT 
                            schedules.kode_mk, schedules.kode_ruang, schedules.kode_dosen, schedules.kelas_id, schedules.jam_mulai, schedules.jam_selesai, schedules.created_at AS tanggal,
                            lecturers.kode AS kode_dosen, lecturers.nama AS nama_dosen, lecturers.id as id_dosen,
                            courses.kode AS kode_mk, courses.nama AS nama_mk, courses.semester, courses.sks,
                            rooms.kode AS kode_ruang, rooms.nama AS nama_ruang,
                            class_times.nama AS nama_kelas 
                        FROM 
                            schedules, lecturers, courses, rooms, class_times
                        WHERE 
                            schedules.kode_dosen = lecturers.kode AND
                            schedules.kode_mk    = courses.kode   AND
                            schedules.kelas_id   = class_times.id AND
                            schedules.kelas_id   = class_times.id AND
                            schedules.kode_mk    = '$kode_mk' AND
                            lecturers.id         = $id")[0];

        $students = DB::select("SELECT 
                            students.nim, students.nama AS nama_mahasiswa, students.semester, students.kode_jurusan,
                            departments.kode AS kode_jurusan, departments.nama AS nama_jurusan,
                            krs.*, 
                            courses.kode AS kode_mk, courses.nama AS nama_mk, courses.id
                        FROM
                            students, departments, krs, courses
                        WHERE 
                            students.kode_jurusan = departments.kode AND 
                            students.kode_jurusan = courses.kode_jurusan AND
                            students.nim = krs.nim AND
                            krs.kode_mk      = courses.kode AND 
                            krs.kode_mk      = '$kode_mk' AND 
                            krs.status_nilai = 1");

        $grades = DB::select("SELECT
                students.nim, students.nama as nama_mahasiswa,
                grades.*,
                courses.kode as kode_mk, courses.sks
                FROM students, grades, courses
                WHERE 
                grades.kode_mk = courses.kode AND
                students.nim = grades.nim AND
                courses.kode = '$course->kode_mk'
            ");

        if (\Auth::user()->role_id == 1 || \Auth::user()->role_id == 2)
            return view('grades.edit', compact('course', 'students', 'grades'));
        else return view('grades_by_lecturer.edit', compact('course', 'students', 'grades'));
    }

    public function set_nilai($student)
    {
        $kode_mk    = $_GET['kode_mk'];
        $kode_dosen = $_GET['kode_dosen'];

        if (\Auth::user()->role_id == 1 || \Auth::user()->role_id == 2) {
            $data = DB::select("SELECT
                students.nim as nim_mahasiswa, students.nama as nama_mahasiswa, students.semester,
                courses.kode kode_mk, courses.nama as nama_mk, courses.sks, courses.semester,
                lecturers.kode as kode_dosen, lecturers.nama as nama_dosen
                FROM students, courses, lecturers
                WHERE 
                    students.nim   = '$student' AND
                    courses.kode   = '$kode_mk' AND
                    lecturers.kode = '$kode_dosen'
            ")[0];

            return view('grades.set_nilai', compact('data'));
        } else {
            $data = DB::select("SELECT
                students.nim as nim_mahasiswa, students.nama as nama_mahasiswa, students.semester,
                courses.kode kode_mk, courses.nama as nama_mk, courses.sks, courses.semester,
                lecturers.kode as kode_dosen, lecturers.nama as nama_dosen,
                krs.*
                FROM students, courses, lecturers, krs
                WHERE 
                    students.nim   = '$student' AND
                    courses.kode   = '$kode_mk' AND
                    krs.kode_mk    = '$kode_mk' AND
                    lecturers.kode = '$kode_dosen'
            ")[0];

            return view('grades_by_lecturer.set_nilai', compact('data'));
        }
    }

    public function update_nilai($student)
    {

        $kode_mk    = $_GET['kode_mk'];
        $kode_dosen = $_GET['kode_dosen'];

        $data = DB::select("SELECT
                students.nim as nim_mahasiswa, students.nama as nama_mahasiswa, students.semester,
                courses.kode kode_mk, courses.nama as nama_mk, courses.sks, courses.semester,
                lecturers.kode as kode_dosen, lecturers.nama as nama_dosen,
                krs.*,
                grades.*
                FROM students, courses, lecturers, krs, grades
                WHERE 
                    students.nim   = '$student' AND
                    courses.kode   = '$kode_mk' AND
                    krs.kode_mk    = '$kode_mk' AND
                    lecturers.kode = '$kode_dosen' AND
                    grades.nim = '$student' AND
                    grades.kode_mk = '$kode_mk'
            ")[0];

        return view('grades_by_lecturer.update_nilai', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Grade  $grade
     * @return \Illuminate\Http\Response
     */
    public function update_now($nim)
    {
        $grade = Grade::where('nim', $nim)->where('kode_mk', request()->kode_mk)->update([
            'presensi' => request()->presensi,
            'tugas'    => request()->tugas,
            'quiz'     => request()->quiz,
            'uts'      => request()->uts,
            'uas'      => request()->uas
        ]);

        return redirect('/grades');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Grade  $grade
     * @return \Illuminate\Http\Response
     */
    public function destroy(Grade $grade)
    {
        //
    }
}
