<?php

namespace App\Http\Controllers;

use App\Schedule;
use App\Department;
use App\Lecturer;
use App\Course;
use App\Class_time;
use App\Room;
use App\Year;
use App\KRS;
use Illuminate\Http\Request;

class ScheduleController extends Controller
{
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departments = Department::all();
        $lecturers = Lecturer::all();
        $courses = Course::all();
        $class_times = Class_time::all();
        $rooms = Room::all();
        $schedules = Schedule::latest()->get();

        if (\Auth::user()->role_id == 1 || \Auth::user()->role_id == 2) 
            return view('schedules.index', compact('schedules', 'departments', 'lecturers', 'courses', 'class_times', 'rooms'));
        else 
            return view('schedules_by_lecturer.index', compact('schedules', 'departments', 'lecturers', 'courses', 'class_times', 'rooms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = Department::all();
        $lecturers   = Lecturer::all();
        $courses     = Course::all();
        $class_times = Class_time::all();
        $rooms       = Room::all();
        $days        = ['Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum\'at', 'Sabtu'];
        $years       = Year::orderBy('tahun', 'asc')->get();

        return view('schedules.create', compact('departments', 'lecturers', 'courses', 'class_times', 'rooms', 'days', 'years'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (\Auth::user()->role_id == 4) {
            Schedule::create($request->all());
        } else {
            $students = \App\Student::where('kode_jurusan', $request->kode_jurusan)->select('nim')->get();
            $schedules = Schedule::create($request->all());

            foreach ($students as $student) {
                $krs = KRS::create([
                    'kode_mk'      => $request->kode_mk,
                    'nim'          => $student->nim,
                    'kode_jurusan' => $request->kode_jurusan,
                    'status_nilai' => 1,
                    'kelas_id'     => $request->kelas_id,
                    'ket'          => 'Baru',
                    'tahun_akad'   => $request->tahun_akademik
                ]);   
            }
        }
        
        return redirect(url('/schedules'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Schedule $schedule)
    {
        $departments = Department::all();
        $lecturers   = Lecturer::all();
        $courses     = Course::all();
        $class_times = Class_time::all();
        $rooms       = Room::all();
        $days        = ['Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum\'at', 'Sabtu'];
        $years       = Year::orderBy('tahun', 'asc')->get();

        if (\Auth::user()->role_id == 1 || \Auth::user()->role_id == 2) 
            return view('schedules.edit', compact('schedule', 'departments', 'lecturers', 'courses', 'class_times', 'rooms', 'days', 'years'));
        else 
            return view('schedules_by_lecturer.edit', compact('schedule', 'departments', 'lecturers', 'courses', 'class_times', 'rooms', 'days', 'years'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Schedule $schedule)
    {

        if (\Auth::user()->role_id == 4) {
            $schedule->update([
                'presensi' => $request->presensi,
                'tugas'    => $request->tugas,
                'quiz'     => $request->quiz,
                'uts'      => $request->uts,
                'uas'      => $request->uas
            ]);

            $krs = KRS::where('kode_mk', $request->kode_mk);
            $krs->update([
                'presensi' => $request->presensi,
                'tugas' => $request->tugas,
                'quiz'  => $request->quiz,
                'uts'   => $request->uts,
                'uas'   => $request->uas 
            ]);
            
        } else $schedule->update($request->all());
        
        return redirect(url('/schedules'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Schedule $schedule)
    {
        $schedule->delete();
        return redirect(url('/schedules'));
    }
}
