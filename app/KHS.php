<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KHS extends Model
{
    protected $table = 'khs';
    protected $guarded = ['_token', '_method'];
}
